{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x86",
			"modernui" : 1
		}
,
		"rect" : [ 35.0, 125.0, 1851.0, 853.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
 ]
					}
,
					"id" : "obj-15",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 1400.0, 367.0, 142.0, 240.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
 ]
					}
,
					"id" : "obj-14",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 1252.5, 367.0, 142.0, 240.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
 ]
					}
,
					"id" : "obj-13",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 1105.0, 367.0, 142.0, 240.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
 ]
					}
,
					"id" : "obj-12",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 957.5, 367.0, 142.0, 240.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
 ]
					}
,
					"id" : "obj-11",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 810.0, 367.0, 142.0, 240.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
 ]
					}
,
					"id" : "obj-10",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 662.5, 367.0, 142.0, 240.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
 ]
					}
,
					"id" : "obj-9",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 515.0, 367.0, 142.0, 240.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
 ]
					}
,
					"id" : "obj-8",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 367.5, 367.0, 142.0, 240.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
 ]
					}
,
					"id" : "obj-6",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 220.0, 367.0, 142.0, 240.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
, 							{
								"filename" : "/Users/nathan/Desktop/Porche/Audio Assets/drips-drops/drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originallengthms" : [ 0.0 ],
									"pitchcorrection" : [ 0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"formantcorrection" : [ 0 ],
									"quality" : [ "basic" ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"pitchshift" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"followglobaltempo" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ]
								}

							}
 ]
					}
,
					"id" : "obj-5",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 72.5, 367.0, 142.0, 240.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1089.0, 58.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 744.5, 266.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "int", "", "", "int" ],
					"patching_rect" : [ 744.5, 229.692383, 81.0, 22.0 ],
					"style" : "",
					"text" : "counter 0 1 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 744.5, 194.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 809.5, 303.0, 113.5, 22.0 ],
					"style" : "",
					"text" : "gate 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 744.5, 157.692383, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 803.5, 93.692383, 29.5, 22.0 ],
					"style" : "",
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 261.0, 763.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 237.0, 723.0, 115.0, 22.0 ],
					"style" : "",
					"text" : "regexp \"clips (\\\\d+)\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "dictionary", "", "", "" ],
					"patching_rect" : [ 226.5, 688.0, 50.5, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0,
						"parameter_enable" : 0
					}
,
					"style" : "",
					"text" : "dict"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 253.0, 652.0, 76.0, 22.0 ],
					"style" : "",
					"text" : "getsize clips"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 253.0, 615.0, 24.0, 22.0 ],
					"style" : "",
					"text" : "t b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 835.5, 29.692383, 29.5, 22.0 ],
					"style" : "",
					"text" : "12"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 803.5, 60.692383, 51.0, 22.0 ],
					"style" : "",
					"text" : "random"
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-3",
					"index" : 2,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 972.0, 739.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-2",
					"index" : 1,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 928.0, 739.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-1",
					"index" : 1,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 798.5, -30.307617, 30.0, 30.0 ],
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"order" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"order" : 1,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-10", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 1 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"source" : [ "obj-105", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"source" : [ "obj-106", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"order" : 1,
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 1 ],
					"order" : 0,
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-11", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-12", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-13", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-14", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-15", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 1 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"order" : 1,
					"source" : [ "obj-5", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"order" : 0,
					"source" : [ "obj-5", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-5", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-6", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-7", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-7", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-7", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-7", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-7", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-7", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-7", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-7", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-7", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-8", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-9", 1 ]
				}

			}
 ]
	}

}
