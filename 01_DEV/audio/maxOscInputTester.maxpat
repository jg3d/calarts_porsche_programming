{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x86",
			"modernui" : 1
		}
,
		"rect" : [ 34.0, 79.0, 825.0, 899.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -971.75, 554.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -971.75, 527.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -653.25, 554.0, 37.0, 22.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ -793.25, 628.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -913.75, 554.0, 210.0, 22.0 ],
					"style" : "",
					"text" : "append sticattoStrings, append Piano"
				}

			}
, 			{
				"box" : 				{
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"id" : "obj-28",
					"items" : [ "sticattoStrings", ",", "Piano" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -793.25, 595.0, 100.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 620.5, 418.4375, 100.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ -793.25, 664.0, 44.0, 22.0 ],
					"style" : "",
					"text" : "gate 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 69.0, 79.0, 1137.0, 899.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-8",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 637.5, 357.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-6",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 479.5, 357.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-5",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 321.5, 357.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/piano_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-4",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 160.5, 357.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 223.5, 204.307617, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 223.5, 168.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "counter 0 1 4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 223.5, 132.307617, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 310.5, 235.307617, 50.5, 22.0 ],
									"style" : "",
									"text" : "gate 4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-112",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 310.5, 99.692383, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 310.5, 67.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 533.5, 701.192383, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 509.5, 661.192383, 115.0, 22.0 ],
									"style" : "",
									"text" : "regexp \"clips (\\\\d+)\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "dictionary", "", "", "" ],
									"patching_rect" : [ 499.0, 626.192383, 50.5, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "dict"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 525.5, 590.192383, 76.0, 22.0 ],
									"style" : "",
									"text" : "getsize clips"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 525.5, 553.192383, 24.0, 22.0 ],
									"style" : "",
									"text" : "t b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 342.5, 3.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "12"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 310.5, 34.692383, 51.0, 22.0 ],
									"style" : "",
									"text" : "random"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 375.0, 590.192383, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 331.0, 590.192383, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 305.5, -56.307617, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 1 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"source" : [ "obj-105", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"source" : [ "obj-106", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 0 ],
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-112", 0 ],
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"order" : 1,
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 1 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"order" : 0,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-5", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-7", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-7", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-8", 1 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ -847.25, 737.5, 209.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher playbackEngineCalArtsPiano"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-250",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2364.75, -5.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-251",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2364.75, -32.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-252",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2733.25, -9.0, 37.0, 22.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-254",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2613.0, -46.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-255",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2543.25, 69.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-256",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2422.75, -5.0, 299.0, 22.0 ],
					"style" : "",
					"text" : "append silverstone, append annon, append grand-prix"
				}

			}
, 			{
				"box" : 				{
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.952941, 0.564706, 0.098039, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"id" : "obj-261",
					"items" : [ "silverstone", ",", "annon", ",", "grand-prix" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2543.25, 40.0, 100.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 785.125, 218.5625, 100.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-179",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2502.75, 660.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-194",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2502.75, 633.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-195",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2821.25, 660.0, 37.0, 22.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-208",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2751.0, 619.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-209",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2681.25, 734.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-215",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2560.75, 660.0, 221.0, 22.0 ],
					"style" : "",
					"text" : "append wet-wind, append howling-wind"
				}

			}
, 			{
				"box" : 				{
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.701961, 0.415686, 0.886275, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"id" : "obj-222",
					"items" : [ "wet-wind", ",", "howling-wind" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2681.25, 705.0, 100.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 785.125, 384.9375, 100.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-178",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2154.0, 646.0, 43.0, 22.0 ],
					"style" : "",
					"text" : "loop 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1740.0, 580.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1740.0, 553.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2108.5, 576.0, 37.0, 22.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1988.25, 539.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-149",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1918.5, 654.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-155",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1798.0, 580.0, 300.0, 22.0 ],
					"style" : "",
					"text" : "append pads, append pizz-strings, append ghost-choir"
				}

			}
, 			{
				"box" : 				{
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.639216, 0.458824, 0.070588, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"id" : "obj-162",
					"items" : [ "pads", ",", "pizz-strings", ",", "ghost-choir" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1918.5, 625.0, 100.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 785.125, 546.5625, 100.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 84.0, 79.0, 1436.0, 853.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 1226.0, 767.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 22.916666,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-15",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 1253.0, 460.0, 164.0, 287.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 1051.0, 767.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 22.916666,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-19",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 1078.0, 460.0, 164.0, 287.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 878.0, 767.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 22.916666,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-21",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 905.0, 460.0, 164.0, 287.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 703.0, 767.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 22.916666,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-23",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 730.0, 460.0, 164.0, 287.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 529.0, 767.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 22.916666,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-11",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 556.0, 460.0, 164.0, 287.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 354.0, 767.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 22.916666,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-13",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 381.0, 460.0, 164.0, 287.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 181.0, 767.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 22.916666,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-6",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 208.0, 460.0, 164.0, 287.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-206",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 6.0, 767.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 22.916666,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-4",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 33.0, 460.0, 164.0, 287.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1089.0, 58.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 744.5, 266.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 744.5, 229.692383, 81.0, 22.0 ],
									"style" : "",
									"text" : "counter 0 1 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 744.5, 194.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 8,
									"outlettype" : [ "", "", "", "", "", "", "", "" ],
									"patching_rect" : [ 809.5, 303.0, 92.5, 22.0 ],
									"style" : "",
									"text" : "gate 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-112",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 744.5, 157.692383, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 803.5, 93.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 193.0, 1047.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 169.0, 1007.0, 115.0, 22.0 ],
									"style" : "",
									"text" : "regexp \"clips (\\\\d+)\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "dictionary", "", "", "" ],
									"patching_rect" : [ 158.5, 972.0, 50.5, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "dict"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 185.0, 936.0, 76.0, 22.0 ],
									"style" : "",
									"text" : "getsize clips"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 185.0, 899.0, 24.0, 22.0 ],
									"style" : "",
									"text" : "t b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 835.5, 29.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "12"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 803.5, 60.692383, 51.0, 22.0 ],
									"style" : "",
									"text" : "random"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 921.0, 957.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 529.0, 972.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 798.5, -30.307617, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-10", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 1 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"source" : [ "obj-105", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"source" : [ "obj-106", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 0 ],
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-112", 0 ],
									"order" : 1,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 1 ],
									"order" : 0,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 1,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-12", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 1 ],
									"order" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-14", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 1 ],
									"order" : 0,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"order" : 1,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-18", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 1 ],
									"order" : 0,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"order" : 1,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-20", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-206", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-206", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 1 ],
									"order" : 0,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"order" : 1,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-22", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 1 ],
									"order" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"order" : 1,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 1 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"order" : 0,
									"source" : [ "obj-4", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-206", 1 ],
									"order" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-206", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"order" : 1,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-5", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"order" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"order" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"order" : 1,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-206", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ -0.75, 741.5, 141.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher dripRandomPan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-193",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -191.75, 550.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-189",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -191.75, 523.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-167",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 126.75, 550.0, 37.0, 22.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.070206, 0.377472, 0.521117, 1.0 ],
					"id" : "obj-8",
					"markers" : [ -60, -48, -36, -24, -12, -6, 0, 6 ],
					"markersused" : 8,
					"maxclass" : "levelmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 29.5, 1070.0, 128.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 518.375, 671.6875, 69.0, 34.5 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.070206, 0.377472, 0.521117, 1.0 ],
					"id" : "obj-226",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -159.25, 616.5, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 446.0, 599.9375, 67.75, 67.75 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-212",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 32.0, 716.5, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 518.375, 623.8125, 67.0, 20.0 ],
					"style" : "",
					"text" : "trigger drip"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 20.0,
					"id" : "obj-210",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -72.0, 452.0, 254.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 444.375, 545.4375, 140.0, 29.0 ],
					"style" : "",
					"text" : "Water Drops",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 56.5, 509.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-165",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ -13.25, 624.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-159",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -133.75, 550.0, 182.0, 22.0 ],
					"style" : "",
					"text" : "append doubleDrip, append drip"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-157",
					"items" : [ "doubleDrip", ",", "drip" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -13.25, 595.0, 100.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 446.0, 576.4375, 100.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.070206, 0.377472, 0.521117, 1.0 ],
					"id" : "obj-153",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -0.75, 716.5, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-151",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ -13.25, 660.0, 44.0, 22.0 ],
					"style" : "",
					"text" : "gate 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-138",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 1251.0, 79.0, 640.0, 829.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 107.5, 400.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-6",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 149.5, 400.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 151.25, 7.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 141.5, 306.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 98.0, 306.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 197.25, 209.0, 35.0, 22.0 ],
									"style" : "",
									"text" : "* 0.6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-180",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 141.5, 355.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-179",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 107.5, 355.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-178",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 98.0, 211.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "1."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-176",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 131.25, 138.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-172",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 131.25, 81.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-166",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 201.5, 108.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-164",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 199.75, 240.5, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-160",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 167.25, 177.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "- 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-159",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 131.25, 108.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-154",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 201.5, 78.0, 42.0, 22.0 ],
									"style" : "",
									"text" : "* 0.35"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-166", 0 ],
									"source" : [ "obj-154", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-176", 0 ],
									"source" : [ "obj-159", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-178", 1 ],
									"order" : 1,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-178", 0 ],
									"order" : 2,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"order" : 0,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"order" : 1,
									"source" : [ "obj-164", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-180", 1 ],
									"order" : 0,
									"source" : [ "obj-164", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-160", 1 ],
									"source" : [ "obj-166", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-159", 0 ],
									"source" : [ "obj-172", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-160", 0 ],
									"source" : [ "obj-176", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-179", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-180", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-154", 0 ],
									"order" : 0,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 0 ],
									"order" : 1,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-164", 0 ],
									"source" : [ "obj-44", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ -0.75, 785.5, 151.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher retweetAdjustAmp"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -93.75, 861.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-92",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -63.75, 928.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -63.75, 893.5, 30.0, 22.0 ],
					"style" : "",
					"text" : "110"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -63.75, 861.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.070206, 0.377472, 0.521117, 1.0 ],
					"id" : "obj-56",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 40.0, 853.0, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.070206, 0.377472, 0.521117, 1.0 ],
					"id" : "obj-57",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 11.0, 853.0, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 593.125, 568.6875, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.070206, 0.377472, 0.521117, 1.0 ],
					"id" : "obj-61",
					"markers" : [ -60, -48, -36, -24, -12, -6, 0, 6 ],
					"markersused" : 8,
					"maxclass" : "levelmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ -112.75, 1070.0, 128.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 444.375, 671.6875, 69.0, 34.5 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 1251.0, 79.0, 640.0, 829.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 107.5, 400.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-6",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 149.5, 400.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 151.25, 7.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 141.5, 306.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 98.0, 306.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 197.25, 209.0, 35.0, 22.0 ],
									"style" : "",
									"text" : "* 0.6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-180",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 141.5, 355.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-179",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 107.5, 355.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-178",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 98.0, 211.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "1."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-176",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 131.25, 138.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-172",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 131.25, 81.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-166",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 201.5, 108.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-164",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 199.75, 240.5, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-160",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 167.25, 177.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "- 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-159",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 131.25, 108.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-154",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 201.5, 78.0, 42.0, 22.0 ],
									"style" : "",
									"text" : "* 0.35"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-166", 0 ],
									"source" : [ "obj-154", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-176", 0 ],
									"source" : [ "obj-159", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-178", 1 ],
									"order" : 1,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-178", 0 ],
									"order" : 2,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"order" : 0,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"order" : 1,
									"source" : [ "obj-164", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-180", 1 ],
									"order" : 0,
									"source" : [ "obj-164", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-160", 1 ],
									"source" : [ "obj-166", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-159", 0 ],
									"source" : [ "obj-172", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-160", 0 ],
									"source" : [ "obj-176", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-179", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-180", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-154", 0 ],
									"order" : 0,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 0 ],
									"order" : 1,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-164", 0 ],
									"source" : [ "obj-44", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ -222.75, 785.5, 151.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher retweetAdjustAmp"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.070206, 0.377472, 0.521117, 1.0 ],
					"id" : "obj-76",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -222.75, 714.5, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 306.0, 317.0, 1436.0, 853.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-12",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 1298.25, 312.0, 166.0, 489.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 1278.25, 815.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-14",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 1121.25, 312.0, 166.0, 489.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 1101.25, 815.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-18",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 944.25, 312.0, 166.0, 489.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 924.25, 815.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-20",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 767.25, 312.0, 166.0, 489.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 747.25, 815.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-10",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 588.0, 312.0, 166.0, 489.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 568.0, 815.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-8",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 411.0, 312.0, 166.0, 489.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 391.0, 815.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-5",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 234.0, 312.0, 166.0, 489.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 214.0, 815.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"channelcount" : 1,
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-4",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 57.0, 312.0, 166.0, 489.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-206",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-8",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-14",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "signal", "bang" ],
													"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
													"style" : "",
													"text" : "line~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "0. 5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "*~"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
													"style" : "",
													"text" : "!- 1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "* 0.001"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
													"style" : "",
													"text" : "random 1000"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 1 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-4", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 37.0, 815.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "patcher randomPan"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1020.75, 30.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 721.25, 215.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 721.25, 178.692383, 81.0, 22.0 ],
									"style" : "",
									"text" : "counter 0 1 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 710.25, 135.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 8,
									"outlettype" : [ "", "", "", "", "", "", "", "" ],
									"patching_rect" : [ 786.25, 252.0, 92.5, 22.0 ],
									"style" : "",
									"text" : "gate 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-112",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 721.25, 106.692383, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 735.25, 65.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 155.0, 1049.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 131.0, 1009.0, 115.0, 22.0 ],
									"style" : "",
									"text" : "regexp \"clips (\\\\d+)\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "dictionary", "", "", "" ],
									"patching_rect" : [ 120.5, 974.0, 50.5, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "dict"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 147.0, 938.0, 76.0, 22.0 ],
									"style" : "",
									"text" : "getsize clips"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 147.0, 901.0, 24.0, 22.0 ],
									"style" : "",
									"text" : "t b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 767.25, 1.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "24"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 735.25, 32.692383, 51.0, 22.0 ],
									"style" : "",
									"text" : "random"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 646.0, 1014.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 602.0, 1014.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 798.5, -30.307617, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 1 ],
									"order" : 0,
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"order" : 1,
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 1 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"source" : [ "obj-105", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"source" : [ "obj-106", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 0 ],
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-112", 0 ],
									"order" : 1,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-11", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 1 ],
									"order" : 0,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"order" : 1,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 1 ],
									"order" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"order" : 1,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-15", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 1 ],
									"order" : 0,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"order" : 1,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-19", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 1 ],
									"order" : 0,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"order" : 1,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-206", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-206", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-21", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 1 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"order" : 0,
									"source" : [ "obj-4", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-206", 1 ],
									"order" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-206", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"order" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 1,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-206", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 1,
									"source" : [ "obj-7", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 2 ],
									"order" : 0,
									"source" : [ "obj-7", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 1 ],
									"order" : 0,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"order" : 1,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-9", 1 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ -222.75, 747.5, 112.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher doubleDrip"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -911.0, 789.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-114",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -881.0, 856.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -881.0, 821.5, 31.0, 22.0 ],
					"style" : "",
					"text" : "108"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -881.0, 789.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -599.0, 934.0, 82.75, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 508.75, 350.8125, 82.75, 20.0 ],
					"style" : "",
					"text" : "If So, What?"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -771.875, 945.0, 55.25, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 508.75, 412.8125, 55.25, 20.0 ],
					"style" : "",
					"text" : "CalArts"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -974.0, 945.0, 59.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 508.75, 476.3125, 59.0, 20.0 ],
					"style" : "",
					"text" : "Porsche"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.30178, 0.283356, 0.224497, 1.0 ],
					"id" : "obj-44",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -559.0, 783.5, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.165741, 0.364658, 0.14032, 1.0 ],
					"id" : "obj-46",
					"knobcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -586.0, 783.5, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 593.25, 350.8125, 22.0, 60.25 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.30178, 0.283356, 0.224497, 1.0 ],
					"id" : "obj-36",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -747.75, 783.5, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.165741, 0.364658, 0.14032, 1.0 ],
					"id" : "obj-40",
					"knobcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -774.75, 783.5, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 593.25, 412.8125, 22.0, 58.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.30178, 0.283356, 0.224497, 1.0 ],
					"id" : "obj-33",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -949.25, 783.5, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.165741, 0.364658, 0.14032, 1.0 ],
					"id" : "obj-32",
					"knobcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -976.25, 783.5, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 593.25, 476.3125, 22.0, 59.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"id" : "obj-24",
					"markers" : [ -60, -48, -36, -24, -12, -6, 0, 6 ],
					"markersused" : 8,
					"maxclass" : "levelmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ -976.25, 970.0, 128.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 508.75, 498.6875, 72.0, 36.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"id" : "obj-25",
					"markers" : [ -60, -48, -36, -24, -12, -6, 0, 6 ],
					"markersused" : 8,
					"maxclass" : "levelmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ -774.75, 970.0, 128.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 508.75, 434.8125, 72.0, 36.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"id" : "obj-26",
					"markers" : [ -60, -48, -36, -24, -12, -6, 0, 6 ],
					"markersused" : 8,
					"maxclass" : "levelmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ -586.0, 964.0, 128.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 508.75, 373.8125, 72.0, 36.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"id" : "obj-38",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -1041.75, 703.5, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 444.375, 476.3125, 59.0, 59.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 345.0, 81.0, 1126.0, 897.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-7",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 655.0, 390.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-6",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 499.0, 390.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-5",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 343.0, 390.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_18.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_17.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_16.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_15.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_14.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_13.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-4",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 187.0, 390.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 290.25, 269.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 290.25, 232.692383, 81.0, 22.0 ],
									"style" : "",
									"text" : "counter 0 1 4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 290.25, 197.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 363.0, 308.0, 50.5, 22.0 ],
									"style" : "",
									"text" : "gate 4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-112",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 290.25, 163.692383, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 348.0, 118.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 181.5, 845.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 157.5, 805.0, 115.0, 22.0 ],
									"style" : "",
									"text" : "regexp \"clips (\\\\d+)\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "dictionary", "", "", "" ],
									"patching_rect" : [ 147.0, 770.0, 50.5, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "dict"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 173.5, 734.0, 76.0, 22.0 ],
									"style" : "",
									"text" : "getsize clips"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 173.5, 697.0, 24.0, 22.0 ],
									"style" : "",
									"text" : "t b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 380.0, 54.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "18"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 348.0, 85.692383, 51.0, 22.0 ],
									"style" : "",
									"text" : "random"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 409.5, 734.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 365.5, 734.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 343.0, -5.307617, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 1 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"source" : [ "obj-105", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"source" : [ "obj-106", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 0 ],
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-112", 0 ],
									"order" : 1,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 1 ],
									"order" : 0,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 1 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"order" : 0,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-5", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-7", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-8", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-8", 3 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ -1041.75, 737.5, 183.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher playbackEnginePorsche"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"id" : "obj-37",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -751.25, 503.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 444.375, 412.8125, 59.0, 59.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"id" : "obj-35",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -623.0, 703.5, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 444.375, 350.8125, 59.0, 59.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 262.0, 82.0, 1163.0, 896.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"clipheight" : 24.75,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-starting_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-starting_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-9",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 583.25, 335.0, 139.0, 206.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 24.75,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-starting_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-starting_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-8",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 435.5, 335.0, 139.0, 206.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 24.75,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-starting_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-starting_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-6",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 287.75, 335.0, 139.0, 206.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 24.75,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-starting_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-starting_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/Desktop/BART/BART Passing/BART-train-passing_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-5",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 140.0, 335.0, 139.0, 206.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 251.5, 240.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 251.5, 203.692383, 81.0, 22.0 ],
									"style" : "",
									"text" : "counter 0 1 4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 251.5, 168.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 316.5, 277.0, 50.5, 22.0 ],
									"style" : "",
									"text" : "gate 4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-112",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 251.5, 131.692383, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 310.5, 67.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 343.5, 713.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 319.5, 673.0, 115.0, 22.0 ],
									"style" : "",
									"text" : "regexp \"clips (\\\\d+)\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "dictionary", "", "", "" ],
									"patching_rect" : [ 309.0, 638.0, 50.5, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "dict"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 335.5, 602.0, 76.0, 22.0 ],
									"style" : "",
									"text" : "getsize clips"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 335.5, 565.0, 24.0, 22.0 ],
									"style" : "",
									"text" : "t b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 342.5, 3.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 310.5, 34.692383, 51.0, 22.0 ],
									"style" : "",
									"text" : "random"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 194.5, 643.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 150.5, 643.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 305.5, -56.307617, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 1 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"source" : [ "obj-105", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"source" : [ "obj-106", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 0 ],
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-112", 0 ],
									"order" : 1,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 1 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"order" : 1,
									"source" : [ "obj-5", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"order" : 0,
									"source" : [ "obj-5", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-5", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-7", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-7", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-7", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-8", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-9", 1 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ -623.0, 737.5, 188.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher playbackEngineIfSoWhat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 69.0, 79.0, 1137.0, 899.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-8",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 533.0, 343.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-6",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 375.0, 343.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-5",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 217.0, 343.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 20.0,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_12.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_11.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_10.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_09.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_08.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events/strings-sticatto_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-4",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 59.0, 343.0, 150.0, 92.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 223.5, 204.307617, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 223.5, 168.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "counter 0 1 4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 223.5, 132.307617, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 310.5, 235.307617, 50.5, 22.0 ],
									"style" : "",
									"text" : "gate 4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-112",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 310.5, 99.692383, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 310.5, 67.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 533.5, 701.192383, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 509.5, 661.192383, 115.0, 22.0 ],
									"style" : "",
									"text" : "regexp \"clips (\\\\d+)\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "dictionary", "", "", "" ],
									"patching_rect" : [ 499.0, 626.192383, 50.5, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "dict"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 525.5, 590.192383, 76.0, 22.0 ],
									"style" : "",
									"text" : "getsize clips"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 525.5, 553.192383, 24.0, 22.0 ],
									"style" : "",
									"text" : "t b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 342.5, 3.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "12"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 310.5, 34.692383, 51.0, 22.0 ],
									"style" : "",
									"text" : "random"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 375.0, 590.192383, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 331.0, 590.192383, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 305.5, -56.307617, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 1 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"source" : [ "obj-105", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"source" : [ "obj-106", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 0 ],
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-112", 0 ],
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"order" : 1,
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 1 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"order" : 0,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-5", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-7", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-7", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-8", 1 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ -865.25, 707.5, 218.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher playbackEngineCalArtsSticatto"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -828.75, 346.0, 59.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 678.125, 467.5625, 70.0, 20.0 ],
					"style" : "",
					"text" : "is retweet?"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"id" : "obj-145",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -828.75, 319.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 620.5, 467.5625, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-150",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ -821.75, 407.0, 158.0, 22.0 ],
					"style" : "",
					"text" : "sel porsche calarts ifsowhat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-161",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -986.0, 407.0, 93.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 791.75, 109.375, 93.0, 22.0 ],
					"style" : "",
					"text" : "print @popup 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-211",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2226.25, 69.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "70"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-216",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2285.25, 69.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 0.0 ],
					"id" : "obj-217",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2206.25, 244.0, 60.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1058.875, 280.875, 60.0, 20.0 ],
					"style" : "",
					"text" : "max amp"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"elementcolor" : [ 0.721195, 0.723978, 0.03182, 1.0 ],
					"floatoutput" : 1,
					"id" : "obj-218",
					"knobcolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ],
					"maxclass" : "slider",
					"min" : 20.0,
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2226.25, 99.5, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 933.375, 166.1875, 20.0, 140.0 ],
					"size" : 137.0,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"format" : 6,
					"id" : "obj-220",
					"maxclass" : "flonum",
					"maximum" : 157.0,
					"minimum" : 20.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2226.25, 268.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1002.125, 280.875, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2609.5, 787.5, 29.5, 22.0 ],
					"style" : "",
					"text" : "70"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-201",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2609.5, 758.75, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 0.0 ],
					"id" : "obj-202",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2589.5, 964.0, 60.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1054.875, 445.5, 60.0, 20.0 ],
					"style" : "",
					"text" : "max amp"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.337303, 0.352808, 0.54972, 1.0 ],
					"elementcolor" : [ 0.461105, 0.492646, 0.591878, 1.0 ],
					"floatoutput" : 1,
					"id" : "obj-204",
					"knobcolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ],
					"maxclass" : "slider",
					"min" : 20.0,
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2609.5, 819.5, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 933.375, 327.875, 20.0, 140.0 ],
					"size" : 137.0,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.337303, 0.352808, 0.54972, 1.0 ],
					"format" : 6,
					"id" : "obj-206",
					"maxclass" : "flonum",
					"maximum" : 157.0,
					"minimum" : 20.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2609.5, 988.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1000.875, 445.5, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-197",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1711.0, 462.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "90"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-199",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1711.0, 429.5, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 0.0 ],
					"id" : "obj-196",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1696.0, 833.0, 60.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1054.875, 608.875, 60.0, 20.0 ],
					"style" : "",
					"text" : "max amp"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 0.0 ],
					"elementcolor" : [ 0.721195, 0.723978, 0.03182, 1.0 ],
					"floatoutput" : 1,
					"id" : "obj-191",
					"knobcolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ],
					"maxclass" : "slider",
					"min" : 20.0,
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1716.0, 688.5, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 933.375, 490.875, 20.0, 140.0 ],
					"size" : 137.0,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 1.0 ],
					"format" : 6,
					"id" : "obj-185",
					"maxclass" : "flonum",
					"maximum" : 157.0,
					"minimum" : 20.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1716.0, 857.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1000.875, 608.875, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 24.0,
					"id" : "obj-139",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -49.75, 41.0, 176.125, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 598.125, 103.875, 232.0, 33.0 ],
					"style" : "",
					"text" : "Matching Term:"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-176",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "list", "list" ],
					"patching_rect" : [ 440.25, 1260.0, 85.0, 22.0 ],
					"style" : "",
					"text" : "omx.peaklim~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-174",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2160.75, 484.5, 41.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1054.875, 585.875, 41.0, 20.0 ],
					"style" : "",
					"text" : "amp"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-173",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2145.75, 469.5, 41.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1052.875, 420.6875, 41.0, 20.0 ],
					"style" : "",
					"text" : "amp"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-172",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1952.0, 218.5, 104.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1057.125, 233.875, 104.0, 20.0 ],
					"style" : "",
					"text" : "tweets per minute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-169",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2130.75, 454.5, 41.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1057.125, 257.875, 41.0, 20.0 ],
					"style" : "",
					"text" : "amp"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1981.5, 47.5, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-146",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2012.5, 80.0, 34.0, 22.0 ],
					"style" : "",
					"text" : "0.85"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-156",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2012.5, 47.5, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-140",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2422.75, 781.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-142",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2453.75, 814.5, 34.0, 22.0 ],
					"style" : "",
					"text" : "0.85"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-143",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2453.75, 781.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1604.0, 433.5, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-131",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1635.0, 500.5, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1635.0, 466.0, 31.0, 22.0 ],
					"style" : "",
					"text" : "100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1635.0, 433.5, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 24.0,
					"id" : "obj-134",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -16.875, 127.5, 143.25, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 637.125, 67.875, 139.0, 33.0 ],
					"style" : "",
					"text" : "Turbulence:"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.721195, 0.723978, 0.03182, 1.0 ],
					"fontsize" : 24.0,
					"format" : 6,
					"id" : "obj-135",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 124.25, 127.5, 90.0, 35.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 793.375, 67.875, 90.0, 35.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 24.0,
					"id" : "obj-133",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -90.0, 83.0, 232.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 559.375, 29.875, 232.0, 33.0 ],
					"style" : "",
					"text" : "Tweets Per Minute:"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.721195, 0.723978, 0.03182, 1.0 ],
					"fontsize" : 24.0,
					"format" : 6,
					"id" : "obj-129",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 124.25, 83.0, 90.0, 35.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 793.375, 27.875, 90.0, 35.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 1.0 ],
					"id" : "obj-91",
					"markers" : [ -60, -48, -36, -24, -12, -6, 0, 6 ],
					"markersused" : 8,
					"maxclass" : "levelmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1854.25, 1028.5, 128.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 785.125, 571.875, 118.0, 59.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.665086, 0.106606, 0.136815, 1.0 ],
					"id" : "obj-89",
					"markers" : [ -60, -48, -36, -24, -12, -6, 0, 6 ],
					"markersused" : 8,
					"maxclass" : "levelmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 648.0, 720.5, 128.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 444.375, 272.8125, 72.0, 36.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 376.0, 169.0, 34.0, 22.0 ],
					"style" : "",
					"text" : "2 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 299.25, 173.0, 34.0, 22.0 ],
					"style" : "",
					"text" : "1 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 299.25, 220.0, 70.0, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0
					}
,
					"style" : "",
					"text" : "coll globals"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"id" : "obj-3",
					"interp" : 199916105559593145992439075707027456.0,
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2330.75, 276.5, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-227",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2064.25, 27.0, 96.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1084.125, 162.875, 67.0, 20.0 ],
					"style" : "",
					"text" : "sensitivity"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"format" : 6,
					"id" : "obj-229",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1969.25, 244.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1002.125, 233.875, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-230",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2049.25, 214.5, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-231",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2059.75, -5.0, 61.0, 20.0 ],
					"style" : "",
					"text" : "new track"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"format" : 6,
					"id" : "obj-232",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2126.25, 474.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1002.125, 257.875, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-233",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2159.25, 342.5, 44.0, 22.0 ],
					"style" : "",
					"text" : "10000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-234",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 2126.25, 408.5, 90.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-235",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "bang" ],
					"patching_rect" : [ 2126.25, 375.5, 36.0, 22.0 ],
					"style" : "",
					"text" : "line~"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"format" : 6,
					"id" : "obj-236",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2087.25, 244.5, 75.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1002.125, 162.875, 75.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"elementcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ],
					"floatoutput" : 1,
					"id" : "obj-238",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2087.25, 64.5, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 961.125, 162.875, 35.25, 140.0 ],
					"size" : 5.0,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-239",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 2064.25, 275.5, 42.0, 22.0 ],
					"style" : "",
					"text" : "* 0.16"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-240",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2098.25, 342.5, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-241",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2126.25, 310.5, 152.0, 22.0 ],
					"style" : "",
					"text" : "if $f1 > $f2 then $2 else $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-242",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1962.5, -95.5, 110.0, 33.0 ],
					"style" : "",
					"text" : "New racetrack sample"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 20.0,
					"id" : "obj-243",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2097.25, -95.5, 137.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 785.125, 166.1875, 125.0, 29.0 ],
					"style" : "",
					"text" : "Race Track",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"id" : "obj-245",
					"interp" : 199916105559593145992439075707027456.0,
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2302.75, 276.5, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 908.125, 162.875, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"id" : "obj-246",
					"markers" : [ -60, -48, -36, -24, -12, -6, 0, 6 ],
					"markersused" : 8,
					"maxclass" : "levelmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2193.75, 432.5, 128.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 785.125, 241.5625, 118.0, 59.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-247",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2285.25, -1.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 41.333332,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/race/275191__konakaboom__2015-british-gt-championship-at-silverstone.mp3",
								"filekind" : "audiofile",
								"loop" : 1,
								"content_state" : 								{
									"originallength" : [ 0.0, "ticks" ],
									"mode" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"quality" : [ "basic" ],
									"speed" : [ 1.0 ],
									"play" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"pitchshift" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ],
									"formant" : [ 1.0 ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/race/jakobthiesen__car-race-ambience.wav",
								"filekind" : "audiofile",
								"loop" : 1,
								"content_state" : 								{
									"originallength" : [ 0.0, "ticks" ],
									"mode" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"quality" : [ "basic" ],
									"speed" : [ 1.0 ],
									"play" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"pitchshift" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ],
									"formant" : [ 1.0 ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/race/john-sipos__florida-grand-prix-in-the-streets-of-st.mp3",
								"filekind" : "audiofile",
								"loop" : 1,
								"content_state" : 								{
									"originallength" : [ 0.0, "ticks" ],
									"mode" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"quality" : [ "basic" ],
									"speed" : [ 1.0 ],
									"play" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"pitchshift" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ],
									"formant" : [ 1.0 ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ]
								}

							}
 ]
					}
,
					"id" : "obj-258",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 2298.0, 128.5, 150.0, 127.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-259",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2122.75, -64.5, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2285.25, 29.0, 43.0, 22.0 ],
					"style" : "",
					"text" : "loop 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-225",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2386.75, 745.0, 96.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 542.125, 197.5625, 69.0, 20.0 ],
					"style" : "",
					"text" : "sensitivity"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.337303, 0.352808, 0.54972, 1.0 ],
					"id" : "obj-224",
					"markers" : [ -60, -48, -36, -24, -12, -6, 0, 6 ],
					"markersused" : 8,
					"maxclass" : "levelmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2688.0, 1157.5, 128.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 785.125, 407.5625, 118.0, 59.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-223",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2468.5, 964.5, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-221",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2494.5, 934.5, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-219",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1635.0, 763.5, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-214",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1575.0, 890.5, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-205",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2509.5, 1157.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1000.875, 420.6875, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 1.0 ],
					"format" : 6,
					"id" : "obj-203",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1635.0, 1027.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1000.875, 584.875, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-198",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1602.5, 763.5, 29.5, 22.0 ],
					"style" : "",
					"text" : "1."
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-192",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1603.0, 808.5, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-181",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1668.0, 921.5, 44.0, 22.0 ],
					"style" : "",
					"text" : "10000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-182",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1635.0, 987.5, 90.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-183",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "bang" ],
					"patching_rect" : [ 1635.0, 954.5, 36.0, 22.0 ],
					"style" : "",
					"text" : "line~"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 1.0 ],
					"format" : 6,
					"id" : "obj-184",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1635.0, 724.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1000.875, 490.875, 77.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-186",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1860.0, 1102.5, 104.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1084.125, 490.875, 62.0, 20.0 ],
					"style" : "",
					"text" : "sensitivity"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 1.0 ],
					"elementcolor" : [ 0.555587, 0.494726, 0.062268, 1.0 ],
					"id" : "obj-187",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1635.0, 535.5, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 959.875, 490.875, 35.25, 140.0 ],
					"size" : 770.0,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-188",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1635.0, 858.5, 42.0, 22.0 ],
					"style" : "",
					"text" : "* 0.16"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-190",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1635.0, 890.5, 152.0, 22.0 ],
					"style" : "",
					"text" : "if $f1 > $f2 then $2 else $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-180",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2542.5, 1058.5, 44.0, 22.0 ],
					"style" : "",
					"text" : "10000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-177",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 2509.5, 1124.5, 90.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-175",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "bang" ],
					"patching_rect" : [ 2509.5, 1091.5, 36.0, 22.0 ],
					"style" : "",
					"text" : "line~"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.337303, 0.352808, 0.54972, 1.0 ],
					"format" : 6,
					"id" : "obj-168",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2532.5, 964.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1002.125, 327.875, 75.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-160",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 604.5, 466.0, 96.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1084.125, 327.875, 69.0, 20.0 ],
					"style" : "",
					"text" : "sensitivity"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.31179, 0.21501, 0.474867, 1.0 ],
					"floatoutput" : 1,
					"id" : "obj-103",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2532.5, 819.5, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 959.875, 327.875, 35.25, 140.0 ],
					"size" : 5.0,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 2509.5, 995.5, 42.0, 22.0 ],
					"style" : "",
					"text" : "* 0.16"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-171",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2481.5, 1058.5, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-164",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2509.5, 1026.5, 152.0, 22.0 ],
					"style" : "",
					"text" : "if $f1 > $f2 then $2 else $1"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 20.0,
					"id" : "obj-6",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2523.25, 580.0, 76.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 785.125, 327.875, 76.0, 29.0 ],
					"style" : "",
					"text" : "Wind",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.337303, 0.352808, 0.54972, 1.0 ],
					"id" : "obj-18",
					"interp" : 199916105559593145992439075707027456.0,
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2681.25, 995.5, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 906.625, 327.875, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"channelcount" : 1,
					"clipheight" : 62.5,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/wet-wind-long-01.wav",
								"filekind" : "audiofile",
								"loop" : 1,
								"content_state" : 								{
									"originallength" : [ 0.0, "ticks" ],
									"mode" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"quality" : [ "basic" ],
									"speed" : [ 1.0 ],
									"play" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"pitchshift" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ],
									"formant" : [ 1.0 ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/howling-wind-01.wav",
								"filekind" : "audiofile",
								"loop" : 1,
								"content_state" : 								{
									"originallength" : [ 0.0, "ticks" ],
									"mode" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"quality" : [ "basic" ],
									"speed" : [ 1.0 ],
									"play" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"pitchshift" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ],
									"formant" : [ 1.0 ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ]
								}

							}
 ]
					}
,
					"id" : "obj-126",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 2681.25, 829.5, 150.0, 127.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-152",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2743.5, 758.75, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-154",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2743.5, 790.75, 43.0, 22.0 ],
					"style" : "",
					"text" : "loop 1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.380952, 0.332597, 0.304988, 1.0 ],
					"fontface" : 1,
					"fontsize" : 20.0,
					"id" : "obj-127",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 339.25, 1468.0, 253.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 855.375, 680.0, 141.0, 29.0 ],
					"style" : "",
					"text" : "Master Gains",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-123",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 353.0, 1179.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 353.0, 1144.5, 31.0, 22.0 ],
					"style" : "",
					"text" : "127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 353.0, 1112.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.380952, 0.332597, 0.304988, 1.0 ],
					"id" : "obj-122",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 443.25, 1414.0, 45.0, 45.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 839.0, 759.0, 45.0, 45.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.380952, 0.332597, 0.304988, 1.0 ],
					"id" : "obj-121",
					"markers" : [ -60, -48, -36, -24, -12, -6, 0, 6 ],
					"markersused" : 8,
					"maxclass" : "levelmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 313.0, 1338.0, 128.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 813.25, 812.0, 79.0, 39.5 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.380952, 0.332597, 0.304988, 1.0 ],
					"id" : "obj-119",
					"markers" : [ -60, -48, -36, -24, -12, -6, 0, 6 ],
					"markersused" : 8,
					"maxclass" : "levelmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 491.25, 1338.0, 128.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 944.625, 814.5, 79.0, 39.5 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.380952, 0.332597, 0.304988, 1.0 ],
					"id" : "obj-117",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 486.25, 1110.0, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 918.625, 714.0, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.380952, 0.332597, 0.304988, 1.0 ],
					"id" : "obj-118",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 459.25, 1110.0, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 894.25, 714.0, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 20.0,
					"id" : "obj-107",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1829.0, 382.0, 78.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 785.125, 490.875, 78.0, 29.0 ],
					"style" : "",
					"text" : "Music",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 299.25, 108.0, 50.0, 47.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1052.875, 394.6875, 104.0, 20.0 ],
					"style" : "",
					"text" : "tweets per minute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 376.0, 109.0, 71.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1053.375, 560.875, 71.0, 20.0 ],
					"style" : "",
					"text" : "/turbulence"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 531.0, 577.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-110",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 562.0, 644.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 562.0, 609.5, 31.0, 22.0 ],
					"style" : "",
					"text" : "127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 562.0, 577.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 1.0 ],
					"id" : "obj-86",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1949.25, 869.5, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 1.0 ],
					"id" : "obj-88",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1922.25, 869.5, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 906.375, 490.875, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 20.0,
					"id" : "obj-80",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 235.25, 307.0, 78.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 444.375, 319.8125, 78.0, 29.0 ],
					"style" : "",
					"text" : "/tweet",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.665086, 0.106606, 0.136815, 1.0 ],
					"id" : "obj-73",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 675.0, 571.0, 22.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.665086, 0.106606, 0.136815, 1.0 ],
					"id" : "obj-75",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 648.0, 571.0, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 518.375, 197.5625, 22.0, 112.125 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 1251.0, 79.0, 640.0, 829.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 107.5, 400.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-6",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 149.5, 400.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 151.25, 7.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 141.5, 306.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 98.0, 306.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 197.25, 209.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "* 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-180",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 141.5, 355.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-179",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 107.5, 355.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-178",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 98.0, 211.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "1."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-176",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 131.25, 138.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-172",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 131.25, 81.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-166",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 201.5, 108.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-164",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 199.75, 240.5, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-160",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 167.25, 177.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "- 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-159",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 131.25, 108.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-154",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 201.5, 78.0, 42.0, 22.0 ],
									"style" : "",
									"text" : "* 0.35"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-166", 0 ],
									"source" : [ "obj-154", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-176", 0 ],
									"source" : [ "obj-159", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-178", 1 ],
									"order" : 1,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-178", 0 ],
									"order" : 2,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"order" : 0,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"order" : 1,
									"source" : [ "obj-164", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-180", 1 ],
									"order" : 0,
									"source" : [ "obj-164", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-160", 1 ],
									"source" : [ "obj-166", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-159", 0 ],
									"source" : [ "obj-172", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-160", 0 ],
									"source" : [ "obj-176", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-179", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-180", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-154", 0 ],
									"order" : 0,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 0 ],
									"order" : 1,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-164", 0 ],
									"source" : [ "obj-44", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 570.0, 535.0, 213.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher retweetAdjustAmpMegaTweet"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 989.0, 79.0, 900.0, 899.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"clipheight" : 29.285715,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-6",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 508.5, 513.0, 150.0, 212.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 29.285715,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-5",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 348.5, 513.0, 150.0, 212.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"clipheight" : 29.285715,
									"data" : 									{
										"clips" : [ 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_07.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_06.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_05.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_04.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_03.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_02.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
, 											{
												"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/Rev-thunder-torpedo_01.wav",
												"filekind" : "audiofile",
												"loop" : 0,
												"content_state" : 												{
													"originallength" : [ 0.0, "ticks" ],
													"mode" : [ "basic" ],
													"formantcorrection" : [ 0 ],
													"originaltempo" : [ 120.0 ],
													"quality" : [ "basic" ],
													"speed" : [ 1.0 ],
													"play" : [ 0 ],
													"slurtime" : [ 0.0 ],
													"pitchshift" : [ 1.0 ],
													"followglobaltempo" : [ 0 ],
													"pitchcorrection" : [ 0 ],
													"formant" : [ 1.0 ],
													"timestretch" : [ 0 ],
													"basictuning" : [ 440 ],
													"originallengthms" : [ 0.0 ]
												}

											}
 ]
									}
,
									"id" : "obj-4",
									"maxclass" : "playlist~",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
									"patching_rect" : [ 188.5, 513.0, 150.0, 212.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 276.5, 353.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 276.5, 315.692383, 81.0, 22.0 ],
									"style" : "",
									"text" : "counter 0 1 3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 276.5, 281.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 419.0, 415.0, 44.0, 22.0 ],
									"style" : "",
									"text" : "gate 3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-112",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 276.5, 248.692383, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 341.5, 212.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 270.5, 890.192383, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 246.5, 850.192383, 115.0, 22.0 ],
									"style" : "",
									"text" : "regexp \"clips (\\\\d+)\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "dictionary", "", "", "" ],
									"patching_rect" : [ 236.0, 815.192383, 50.5, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "dict"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 262.5, 779.192383, 76.0, 22.0 ],
									"style" : "",
									"text" : "getsize clips"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 262.5, 742.192383, 24.0, 22.0 ],
									"style" : "",
									"text" : "t b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 373.5, 148.692383, 29.5, 22.0 ],
									"style" : "",
									"text" : "7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 341.5, 179.692383, 51.0, 22.0 ],
									"style" : "",
									"text" : "random"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 280.5, 952.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 236.5, 952.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 336.5, 88.692383, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 1 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"source" : [ "obj-105", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"source" : [ "obj-106", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 0 ],
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-112", 0 ],
									"order" : 1,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 1 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"order" : 0,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-5", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-7", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 570.0, 497.0, 139.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher playbackEngine"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71909, 0.639109, 0.045021, 1.0 ],
					"format" : 6,
					"id" : "obj-132",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 376.0, 134.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1000.875, 560.875, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.337303, 0.352808, 0.54972, 1.0 ],
					"format" : 6,
					"id" : "obj-130",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 299.25, 134.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1000.875, 394.6875, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"format" : 6,
					"id" : "obj-64",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 357.25, 329.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 620.5, 515.5625, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"format" : 6,
					"id" : "obj-65",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 285.25, 336.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 620.5, 491.5625, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"format" : 6,
					"id" : "obj-67",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 144.25, 355.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 620.5, 443.5625, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 144.25, 381.0, 70.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 678.125, 458.5625, 70.0, 20.0 ],
					"style" : "",
					"text" : "sentiment"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 357.25, 357.0, 55.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 678.125, 516.5625, 55.0, 20.0 ],
					"style" : "",
					"text" : "strength"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 285.25, 366.0, 57.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 678.125, 493.5625, 57.0, 20.0 ],
					"style" : "",
					"text" : "distance"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.665086, 0.106606, 0.136815, 1.0 ],
					"format" : 6,
					"id" : "obj-63",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 710.0, 392.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 542.125, 287.5625, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.665086, 0.106606, 0.136815, 1.0 ],
					"format" : 6,
					"id" : "obj-60",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 640.0, 390.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 542.125, 264.5625, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.665086, 0.106606, 0.136815, 1.0 ],
					"format" : 6,
					"id" : "obj-58",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 487.0, 390.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 542.125, 218.5625, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 487.0, 416.5, 70.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 603.125, 218.5625, 70.0, 20.0 ],
					"style" : "",
					"text" : "sentiment"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 570.0, 417.5, 59.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 603.125, 241.5625, 70.0, 20.0 ],
					"style" : "",
					"text" : "is retweet?"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 710.0, 420.5, 55.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 603.125, 287.5625, 55.0, 20.0 ],
					"style" : "",
					"text" : "strength"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 640.0, 420.5, 57.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 603.125, 264.5625, 57.0, 20.0 ],
					"style" : "",
					"text" : "distance"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 20.0,
					"id" : "obj-39",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 582.0, 357.5, 126.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 444.375, 166.1875, 126.0, 29.0 ],
					"style" : "",
					"text" : "/megatweet",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 31.166666,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Background Tracks/Porsche-Brainstorming-ambiant-ocean-without-car-sounds.wav",
								"filekind" : "audiofile",
								"loop" : 1,
								"content_state" : 								{
									"originallength" : [ 0.0, "ticks" ],
									"mode" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"quality" : [ "basic" ],
									"speed" : [ 1.0 ],
									"play" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"pitchshift" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ],
									"formant" : [ 1.0 ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Background Tracks/orchestrated-pizz-strings.wav",
								"filekind" : "audiofile",
								"loop" : 1,
								"content_state" : 								{
									"originallength" : [ 0.0, "ticks" ],
									"mode" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"quality" : [ "basic" ],
									"speed" : [ 1.0 ],
									"play" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"pitchshift" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ],
									"formant" : [ 1.0 ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Background Tracks/ambiant-ghost-choir_01.wav",
								"filekind" : "audiofile",
								"loop" : 1,
								"content_state" : 								{
									"originallength" : [ 0.0, "ticks" ],
									"mode" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"quality" : [ "basic" ],
									"speed" : [ 1.0 ],
									"play" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"pitchshift" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ],
									"formant" : [ 1.0 ],
									"timestretch" : [ 0 ],
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ]
								}

							}
 ]
					}
,
					"id" : "obj-10",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 1921.0, 753.5, 223.5, 96.5 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.665086, 0.106606, 0.136815, 1.0 ],
					"id" : "obj-79",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 570.0, 390.5, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 542.125, 241.5625, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "float", "int", "float", "float" ],
					"patching_rect" : [ 570.0, 323.0, 109.0, 22.0 ],
					"style" : "",
					"text" : "unpack x 0. 0 0. 0."
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.665086, 0.106606, 0.136815, 1.0 ],
					"id" : "obj-71",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 570.0, 464.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 444.375, 197.5625, 70.625, 70.625 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4.0, 323.0, 93.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 791.75, 109.375, 93.0, 22.0 ],
					"style" : "",
					"text" : "print @popup 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "float", "int", "float", "float" ],
					"patching_rect" : [ 228.0, 269.0, 109.0, 22.0 ],
					"style" : "",
					"text" : "unpack x 0. 0 0. 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 313.0, 30.0, 99.0, 22.0 ],
					"style" : "",
					"text" : "udpreceive 7000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 313.0, 74.0, 271.0, 22.0 ],
					"style" : "",
					"text" : "OSC-route /speed /turbulence /tweet /megatweet"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"order" : 1,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"order" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-1", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"source" : [ "obj-1", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-10", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 0 ],
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-11", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 0 ],
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"order" : 2,
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"order" : 1,
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"order" : 0,
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 0 ],
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"source" : [ "obj-116", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-176", 1 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-118", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-176", 0 ],
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"source" : [ "obj-125", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"order" : 0,
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-229", 0 ],
					"order" : 1,
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"order" : 2,
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-187", 0 ],
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"order" : 1,
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 0 ],
					"order" : 2,
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-192", 0 ],
					"order" : 0,
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 0 ],
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-137", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"source" : [ "obj-138", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"source" : [ "obj-138", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-143", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 0 ],
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 2 ],
					"order" : 0,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 2 ],
					"order" : 1,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-238", 0 ],
					"source" : [ "obj-146", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 0 ],
					"source" : [ "obj-150", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-150", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-150", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-150", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-153", 0 ],
					"source" : [ "obj-151", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-151", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-154", 0 ],
					"source" : [ "obj-152", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"source" : [ "obj-154", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 0 ],
					"source" : [ "obj-156", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 0 ],
					"source" : [ "obj-157", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"source" : [ "obj-159", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"source" : [ "obj-16", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-150", 0 ],
					"order" : 1,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 0 ],
					"order" : 2,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"order" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"source" : [ "obj-16", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"source" : [ "obj-16", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"source" : [ "obj-16", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 0 ],
					"source" : [ "obj-162", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 0 ],
					"order" : 2,
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"order" : 1,
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 0 ],
					"order" : 0,
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 0 ],
					"order" : 1,
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"order" : 0,
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"order" : 5,
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-189", 0 ],
					"order" : 2,
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"order" : 3,
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"order" : 4,
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"order" : 0,
					"source" : [ "obj-168", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-221", 0 ],
					"order" : 1,
					"source" : [ "obj-168", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 0 ],
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"order" : 0,
					"source" : [ "obj-176", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"order" : 1,
					"source" : [ "obj-176", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 1 ],
					"order" : 1,
					"source" : [ "obj-176", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"order" : 0,
					"source" : [ "obj-176", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"order" : 0,
					"source" : [ "obj-177", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-205", 0 ],
					"order" : 1,
					"source" : [ "obj-177", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-178", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-222", 0 ],
					"source" : [ "obj-179", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"order" : 1,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"order" : 2,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-224", 0 ],
					"order" : 0,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 1 ],
					"source" : [ "obj-180", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 1 ],
					"source" : [ "obj-181", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-203", 0 ],
					"order" : 1,
					"source" : [ "obj-182", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"order" : 0,
					"source" : [ "obj-182", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-182", 0 ],
					"source" : [ "obj-183", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-188", 1 ],
					"order" : 0,
					"source" : [ "obj-184", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-219", 0 ],
					"order" : 1,
					"source" : [ "obj-184", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-190", 1 ],
					"source" : [ "obj-185", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-184", 0 ],
					"source" : [ "obj-187", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-190", 0 ],
					"order" : 0,
					"source" : [ "obj-188", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-214", 0 ],
					"order" : 1,
					"source" : [ "obj-188", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"source" : [ "obj-189", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-181", 0 ],
					"order" : 0,
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 0 ],
					"order" : 1,
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 0 ],
					"source" : [ "obj-191", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-188", 0 ],
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"source" : [ "obj-193", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-179", 0 ],
					"source" : [ "obj-194", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-222", 0 ],
					"source" : [ "obj-195", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-191", 0 ],
					"source" : [ "obj-197", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-192", 0 ],
					"source" : [ "obj-198", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-197", 0 ],
					"source" : [ "obj-199", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-204", 0 ],
					"source" : [ "obj-200", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-200", 0 ],
					"source" : [ "obj-201", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 0 ],
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 1 ],
					"source" : [ "obj-206", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-194", 0 ],
					"order" : 2,
					"source" : [ "obj-208", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-195", 0 ],
					"order" : 0,
					"source" : [ "obj-208", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-215", 0 ],
					"order" : 1,
					"source" : [ "obj-208", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-218", 0 ],
					"source" : [ "obj-211", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 1 ],
					"source" : [ "obj-213", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 0 ],
					"source" : [ "obj-213", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-222", 0 ],
					"source" : [ "obj-215", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-211", 0 ],
					"source" : [ "obj-216", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-220", 0 ],
					"source" : [ "obj-218", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-192", 0 ],
					"source" : [ "obj-219", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 1 ],
					"source" : [ "obj-22", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-241", 1 ],
					"source" : [ "obj-220", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-221", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-209", 0 ],
					"source" : [ "obj-222", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-223", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 1 ],
					"source" : [ "obj-226", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 0 ],
					"source" : [ "obj-229", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 0 ],
					"source" : [ "obj-230", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 1 ],
					"source" : [ "obj-233", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-232", 0 ],
					"order" : 1,
					"source" : [ "obj-234", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-245", 0 ],
					"order" : 0,
					"source" : [ "obj-234", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-234", 0 ],
					"source" : [ "obj-235", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 0 ],
					"order" : 1,
					"source" : [ "obj-236", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 1 ],
					"order" : 0,
					"source" : [ "obj-236", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 0 ],
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-241", 0 ],
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-233", 0 ],
					"order" : 0,
					"source" : [ "obj-241", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 0 ],
					"order" : 1,
					"source" : [ "obj-241", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-240", 0 ],
					"order" : 2,
					"source" : [ "obj-241", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"order" : 1,
					"source" : [ "obj-245", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 0 ],
					"order" : 0,
					"source" : [ "obj-245", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-245", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 0 ],
					"source" : [ "obj-247", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 0 ],
					"source" : [ "obj-250", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-250", 0 ],
					"source" : [ "obj-251", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 0 ],
					"source" : [ "obj-252", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-251", 0 ],
					"order" : 2,
					"source" : [ "obj-254", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-252", 0 ],
					"order" : 0,
					"source" : [ "obj-254", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-256", 0 ],
					"order" : 1,
					"source" : [ "obj-254", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-258", 0 ],
					"source" : [ "obj-255", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 0 ],
					"source" : [ "obj-256", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-245", 0 ],
					"source" : [ "obj-258", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-258", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-247", 0 ],
					"source" : [ "obj-259", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-258", 0 ],
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-255", 0 ],
					"source" : [ "obj-261", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-29", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-31", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"order" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"order" : 1,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"source" : [ "obj-32", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-34", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 1 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"order" : 0,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"order" : 1,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-40", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"source" : [ "obj-43", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"order" : 0,
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"order" : 1,
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-46", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-51", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"order" : 0,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"order" : 1,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"order" : 0,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"source" : [ "obj-57", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"order" : 1,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"source" : [ "obj-62", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"order" : 1,
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-75", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"order" : 0,
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 3,
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"order" : 2,
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"order" : 0,
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"order" : 1,
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 2 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"order" : 1,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-81", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-81", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-81", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"order" : 0,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"source" : [ "obj-81", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 1 ],
					"source" : [ "obj-85", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"order" : 1,
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-88", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"order" : 0,
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"source" : [ "obj-95", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "Porsche-Brainstorming-ambiant-ocean-without-car-sounds.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Background Tracks",
				"patcherrelativepath" : "./audio-assets/Background Tracks",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "orchestrated-pizz-strings.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Background Tracks",
				"patcherrelativepath" : "./audio-assets/Background Tracks",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "ambiant-ghost-choir_01.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Background Tracks",
				"patcherrelativepath" : "./audio-assets/Background Tracks",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "Rev-thunder-torpedo_07.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "Rev-thunder-torpedo_06.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "Rev-thunder-torpedo_05.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "Rev-thunder-torpedo_04.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "Rev-thunder-torpedo_03.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "Rev-thunder-torpedo_02.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "Rev-thunder-torpedo_01.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "wet-wind-long-01.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets",
				"patcherrelativepath" : "./audio-assets",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "howling-wind-01.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets",
				"patcherrelativepath" : "./audio-assets",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "275191__konakaboom__2015-british-gt-championship-at-silverstone.mp3",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/race",
				"patcherrelativepath" : "./audio-assets/race",
				"type" : "Mp3 ",
				"implicit" : 1
			}
, 			{
				"name" : "jakobthiesen__car-race-ambience.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/race",
				"patcherrelativepath" : "./audio-assets/race",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "john-sipos__florida-grand-prix-in-the-streets-of-st.mp3",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/race",
				"patcherrelativepath" : "./audio-assets/race",
				"type" : "Mp3 ",
				"implicit" : 1
			}
, 			{
				"name" : "globals",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio",
				"patcherrelativepath" : ".",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_12.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_11.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_10.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_09.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_08.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_07.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_06.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_05.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_04.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_03.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_02.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "strings-sticatto_01.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "BART-train-starting_02.wav",
				"bootpath" : "~/Desktop/BART/BART Passing",
				"patcherrelativepath" : "../../../../Desktop/BART/BART Passing",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "BART-train-starting_01.wav",
				"bootpath" : "~/Desktop/BART/BART Passing",
				"patcherrelativepath" : "../../../../Desktop/BART/BART Passing",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "BART-train-passing_06.wav",
				"bootpath" : "~/Desktop/BART/BART Passing",
				"patcherrelativepath" : "../../../../Desktop/BART/BART Passing",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "BART-train-passing_05.wav",
				"bootpath" : "~/Desktop/BART/BART Passing",
				"patcherrelativepath" : "../../../../Desktop/BART/BART Passing",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "BART-train-passing_04.wav",
				"bootpath" : "~/Desktop/BART/BART Passing",
				"patcherrelativepath" : "../../../../Desktop/BART/BART Passing",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "BART-train-passing_03.wav",
				"bootpath" : "~/Desktop/BART/BART Passing",
				"patcherrelativepath" : "../../../../Desktop/BART/BART Passing",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "BART-train-passing_02.wav",
				"bootpath" : "~/Desktop/BART/BART Passing",
				"patcherrelativepath" : "../../../../Desktop/BART/BART Passing",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "BART-train-passing_01.wav",
				"bootpath" : "~/Desktop/BART/BART Passing",
				"patcherrelativepath" : "../../../../Desktop/BART/BART Passing",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_18.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_17.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_16.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_15.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_14.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_13.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_12.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_11.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_10.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_09.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_08.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_07.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_06.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_05.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_04.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_03.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_02.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sputter-explosion_01.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events",
				"patcherrelativepath" : "./audio-assets/Tweet Events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_24.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_23.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_22.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_21.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_20.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_19.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_18.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_17.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_16.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_15.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_14.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_13.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_12.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_11.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_10.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_09.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_08.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_07.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_06.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_05.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_04.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_03.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_02.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "double_drip_01.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_12.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_11.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_10.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_09.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_08.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_07.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_06.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_05.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_04.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_03.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_02.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "drip_01.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips",
				"patcherrelativepath" : "./audio-assets/drips",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_12.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_11.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_10.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_09.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_08.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_07.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_06.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_05.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_04.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_03.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_02.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "piano_01.wav",
				"bootpath" : "~/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/calarts-events",
				"patcherrelativepath" : "./audio-assets/Tweet Events/calarts-events",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "OSC-route.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0
	}

}
