{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x86",
			"modernui" : 1
		}
,
		"rect" : [ 51.0, 79.0, 1436.0, 853.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"channelcount" : 1,
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
 ]
					}
,
					"id" : "obj-12",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 1298.25, 312.0, 166.0, 489.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-16",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
									"style" : "",
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "!- 1."
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
									"style" : "",
									"text" : "* 0.001"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "random 1000"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1278.25, 815.0, 116.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher randomPan"
				}

			}
, 			{
				"box" : 				{
					"channelcount" : 1,
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
 ]
					}
,
					"id" : "obj-14",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 1121.25, 312.0, 166.0, 489.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-16",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
									"style" : "",
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "!- 1."
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
									"style" : "",
									"text" : "* 0.001"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "random 1000"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1101.25, 815.0, 116.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher randomPan"
				}

			}
, 			{
				"box" : 				{
					"channelcount" : 1,
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
 ]
					}
,
					"id" : "obj-18",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 944.25, 312.0, 166.0, 489.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-16",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
									"style" : "",
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "!- 1."
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
									"style" : "",
									"text" : "* 0.001"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "random 1000"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 924.25, 815.0, 116.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher randomPan"
				}

			}
, 			{
				"box" : 				{
					"channelcount" : 1,
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
 ]
					}
,
					"id" : "obj-20",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 767.25, 312.0, 166.0, 489.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-16",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
									"style" : "",
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "!- 1."
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
									"style" : "",
									"text" : "* 0.001"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "random 1000"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 747.25, 815.0, 116.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher randomPan"
				}

			}
, 			{
				"box" : 				{
					"channelcount" : 1,
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
 ]
					}
,
					"id" : "obj-10",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 588.0, 312.0, 166.0, 489.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-16",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
									"style" : "",
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "!- 1."
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
									"style" : "",
									"text" : "* 0.001"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "random 1000"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 568.0, 815.0, 116.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher randomPan"
				}

			}
, 			{
				"box" : 				{
					"channelcount" : 1,
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
 ]
					}
,
					"id" : "obj-8",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 411.0, 312.0, 166.0, 489.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-16",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
									"style" : "",
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "!- 1."
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
									"style" : "",
									"text" : "* 0.001"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "random 1000"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 391.0, 815.0, 116.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher randomPan"
				}

			}
, 			{
				"box" : 				{
					"channelcount" : 1,
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
 ]
					}
,
					"id" : "obj-5",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 234.0, 312.0, 166.0, 489.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-16",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
									"style" : "",
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "!- 1."
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
									"style" : "",
									"text" : "* 0.001"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "random 1000"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 214.0, 815.0, 116.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher randomPan"
				}

			}
, 			{
				"box" : 				{
					"channelcount" : 1,
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_24.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_23.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_22.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_21.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_20.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_19.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/drips/double_drip_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"quality" : [ "basic" ],
									"originaltempo" : [ 120.0 ],
									"formant" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"formantcorrection" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"basictuning" : [ 440 ],
									"timestretch" : [ 0 ],
									"speed" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"play" : [ 0 ],
									"mode" : [ "basic" ],
									"originallength" : [ 0.0, "ticks" ],
									"followglobaltempo" : [ 0 ],
									"pitchcorrection" : [ 0 ]
								}

							}
 ]
					}
,
					"id" : "obj-4",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 57.0, 312.0, 166.0, 489.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-206",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-16",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 269.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 232.0, 265.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 338.0, 19.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 160.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 117.0, 160.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 246.0, 182.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 283.0, 204.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 246.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.0, 182.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "0. 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 246.0, 72.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 269.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 232.0, 226.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "*~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 246.0, 138.0, 55.5, 22.0 ],
									"style" : "",
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 283.0, 160.0, 32.5, 22.0 ],
									"style" : "",
									"text" : "!- 1."
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 246.0, 116.0, 49.0, 22.0 ],
									"style" : "",
									"text" : "* 0.001"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 246.0, 94.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "random 1000"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 37.0, 815.0, 116.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "patcher randomPan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1020.75, 30.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 721.25, 215.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "int", "", "", "int" ],
					"patching_rect" : [ 721.25, 178.692383, 81.0, 22.0 ],
					"style" : "",
					"text" : "counter 0 1 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 710.25, 135.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"outlettype" : [ "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 786.25, 252.0, 92.5, 22.0 ],
					"style" : "",
					"text" : "gate 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 721.25, 106.692383, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 735.25, 65.692383, 29.5, 22.0 ],
					"style" : "",
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 155.0, 1049.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 131.0, 1009.0, 115.0, 22.0 ],
					"style" : "",
					"text" : "regexp \"clips (\\\\d+)\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "dictionary", "", "", "" ],
					"patching_rect" : [ 120.5, 974.0, 50.5, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0,
						"parameter_enable" : 0
					}
,
					"style" : "",
					"text" : "dict"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 147.0, 938.0, 76.0, 22.0 ],
					"style" : "",
					"text" : "getsize clips"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 147.0, 901.0, 24.0, 22.0 ],
					"style" : "",
					"text" : "t b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 767.25, 1.692383, 29.5, 22.0 ],
					"style" : "",
					"text" : "24"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 735.25, 32.692383, 51.0, 22.0 ],
					"style" : "",
					"text" : "random"
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-3",
					"index" : 2,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 646.0, 1014.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-2",
					"index" : 1,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 602.0, 1014.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-1",
					"index" : 1,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 798.5, -30.307617, 30.0, 30.0 ],
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"order" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"order" : 1,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 1 ],
					"order" : 0,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"order" : 1,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 1 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"source" : [ "obj-105", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"source" : [ "obj-106", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"order" : 1,
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 1 ],
					"order" : 0,
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"order" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"order" : 1,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 1 ],
					"order" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 1,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 1 ],
					"order" : 0,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"order" : 1,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 1 ],
					"order" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"order" : 1,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-206", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-206", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 1 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"order" : 1,
					"source" : [ "obj-4", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"order" : 0,
					"source" : [ "obj-4", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 1 ],
					"order" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 0 ],
					"order" : 1,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 1 ],
					"order" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"order" : 1,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-6", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-7", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-7", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-7", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-7", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-7", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 2 ],
					"order" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 1,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-7", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-7", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 1 ],
					"order" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"order" : 1,
					"source" : [ "obj-8", 0 ]
				}

			}
 ]
	}

}
