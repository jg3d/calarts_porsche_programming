{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x86",
			"modernui" : 1
		}
,
		"rect" : [ 345.0, 81.0, 1541.0, 897.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
 ]
					}
,
					"id" : "obj-7",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 655.0, 390.0, 150.0, 92.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
 ]
					}
,
					"id" : "obj-6",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 499.0, 390.0, 150.0, 92.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
 ]
					}
,
					"id" : "obj-5",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 343.0, 390.0, 150.0, 92.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 20.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_18.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_17.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_16.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_15.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_14.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_13.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_12.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_11.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_10.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_09.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_08.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_07.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_06.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_05.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_04.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_03.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_02.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "/Users/nathan/workspace/calarts_porsche_programming/01_DEV/audio/audio-assets/Tweet Events/sputter-explosion_01.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"basictuning" : [ 440 ],
									"originallengthms" : [ 0.0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"originaltempo" : [ 120.0 ],
									"followglobaltempo" : [ 0 ],
									"slurtime" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"formant" : [ 1.0 ],
									"pitchshift" : [ 1.0 ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"timestretch" : [ 0 ],
									"play" : [ 0 ],
									"speed" : [ 1.0 ]
								}

							}
 ]
					}
,
					"id" : "obj-4",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 187.0, 390.0, 150.0, 92.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 290.25, 269.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "int", "", "", "int" ],
					"patching_rect" : [ 290.25, 232.692383, 81.0, 22.0 ],
					"style" : "",
					"text" : "counter 0 1 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 290.25, 197.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 363.0, 308.0, 50.5, 22.0 ],
					"style" : "",
					"text" : "gate 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 290.25, 163.692383, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 348.0, 118.692383, 29.5, 22.0 ],
					"style" : "",
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 181.5, 845.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 157.5, 805.0, 115.0, 22.0 ],
					"style" : "",
					"text" : "regexp \"clips (\\\\d+)\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "dictionary", "", "", "" ],
					"patching_rect" : [ 147.0, 770.0, 50.5, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0,
						"parameter_enable" : 0
					}
,
					"style" : "",
					"text" : "dict"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 173.5, 734.0, 76.0, 22.0 ],
					"style" : "",
					"text" : "getsize clips"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 173.5, 697.0, 24.0, 22.0 ],
					"style" : "",
					"text" : "t b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 380.0, 54.692383, 29.5, 22.0 ],
					"style" : "",
					"text" : "18"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 348.0, 85.692383, 51.0, 22.0 ],
					"style" : "",
					"text" : "random"
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-3",
					"index" : 2,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 409.5, 734.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-2",
					"index" : 1,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 365.5, 734.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-1",
					"index" : 1,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 343.0, -5.307617, 30.0, 30.0 ],
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"order" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"order" : 1,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 1 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"source" : [ "obj-105", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"source" : [ "obj-106", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"order" : 1,
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 1 ],
					"order" : 0,
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 1 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"order" : 1,
					"source" : [ "obj-4", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"order" : 0,
					"source" : [ "obj-4", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-5", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-6", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-7", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-8", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-8", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-8", 3 ]
				}

			}
 ]
	}

}
