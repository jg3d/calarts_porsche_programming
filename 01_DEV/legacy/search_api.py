
def searchAllTerms(terms, num, func_print=True):
    """
    INPUTS
    -------------------
    terms           list of strings
                    all of the terms to search twitter for

    num             int
                    the number of search results to return for each term

    OUTPUTS
    -------------------
    NONE
    """
    tweets =[]

    # search for tweets which match all of the terms
    for term in terms:
        if func_print is True:
            print("Searching for term : ", term)

        # the try/except will handle the rate limit
        try:
            tweets.extend(sentiment.translateTweets(searchForTerm(term, num)))
        except tweepy.TweepError:
            # TODO THIS IS A LITTLE FUNKY AFTER THE REVISION
            print("overeached the rate limit, pausing program for 5 seconds")
            time.sleep(5)
            break

    if func_print is True:
        print("returned ", len(tweets), "tweets")
    # remove tweets which appeared in multiple searches
    tweets = removeDuplicateTweets(tweets)
    # remove tweets which have been used for OSC messages already
    tweets = removeUsedTweets(tweets)

    # now lets look at all the tweets
    for tweet in tweets:
        tweet.distance = findTweetLocation(tweet) # try to figure out if there is any location information
        tweet.sentiment = getTweetSentiment(tweet) # get tweet sentiment
        tweet.terms = findTweetTerms(tweet, terms) # recheck the tweet's text to see what keywords they match
        sendCorrectOscMessage(tweet)


def printRateLimit():
    rate_limit_data = api.rate_limit_status()
    print("----------------------------------")
    print("rate limit : ",
            rate_limit_data['resources']['search']['/search/tweets']['remaining'], "/",
            rate_limit_data['resources']['search']['/search/tweets']['limit'])
    print("----------------------------------")


def searchForTerm(term, num):
    ittr = tweepy.Cursor(api.search, q=term).items(num)
    matching_tweets = []
    for t in ittr:
        matching_tweets.append(t)
    return matching_tweets

from keys import *

