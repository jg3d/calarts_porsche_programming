import tweepy
import sentiment
import argparse
import time
import tweetUtils
import urllib
from threading import Thread
from keys import *
from os import path
from pythonosc import osc_message_builder
from pythonosc import udp_client


def adjustStoredTweetList(used_tweets):
    """
    ensures that we are never keeping more than 2000 tweets in the
    used_tweets list (to prevent a memory leak). If more than 2000
    tweets are detected we will remove the first 300 tweets in the
    list as they are the oldest and are less likely to reappear


    INPUTS
    -----------
    used_tweets         list of tweet objects

    OUTPUTS
    -----------
    used_tweets         list of tweet objects

    """
    if len(used_tweets) > 2000:
        used_tweets = used_tweets[300:]
    return used_tweets


def getTweetSentiment(tweet):
    """
    this function runs the sentiment analysis code in sentiment.py.
    it will also print out the text of tweets which score below -0.5
    """
    # sentiment
    sent = sentiment.tweetSentiment(tweet)
    return sent


def determineTweetStrength(tweet):
    """
    returns the strength of a megatweet to be used when sending the message
    """

    # the base strength is determined by how many terms were matched
    # the tweet gets one point for each keyterm
    strength = 0
    for term in tweet.terms:
        for word in tweet.og_text.split(" "):
            if term.lower() in word.lower():
                strength += 1

    # The tweet then receives 0.75 points for each secondary term present
    for sterm in args.sterms:
        for word in tweet.og_text.split(" "):
            if sterm.lower() in word.lower():
                strength += 0.75


    # if a #keyword is found the tweet will receive an extra 1.5 points
    for term in args.terms:
        if "#"+term.lower() in tweet.og_text.lower():
            strength += 1.5

    # for every mile under 10 times the strength by its inverse
    # e.g. 5 miles away is 5x, 1 mile away is 9x
    if tweet.distance < 5.0 and tweet.distance > -1.0:
        strength = strength * (6.0 - tweet.distance)

    if strength > 1 and args.verbose is True:
        print("strong tweet detected : ", strength, tweet.og_text)

    return strength


def buildOscString(prepend, tweet):
    """
    This function creates a string representation of
    an OSC message for the purpose of printing.

    INPUTS
    ------
    prepend     str` The osc message /prepend portion
    tweet       tweet object` The tweet object which all other aspects of
                the OSC message are created from.

    OUTPUTS
    -------
    stringrep   str` The string representation of the OSC message
    """
    stringrep = str(tweet.last_time) + ","
    stringrep += prepend + ","
    terms = ''
    for term in tweet.terms:
        terms += term + " "
    stringrep += terms + ", "
    stringrep += str(tweet.sentiment) + ", "
    stringrep += str(tweet.retweeted) + ", "
    stringrep += str(tweet.distance) + ", "
    # stringrep += str(tweet.strength) + "\n"
    return stringrep


def addStringToFile(astr):
    """
    This simple utility function opens up the unique log file
    created for the program at the start of run-time and writes
    strings to the file.

    INPUTS
    ------
    astr    `str    The string which will be written to the log file

    RETURNS
    -------
    NONE
    """
    with open(file_name, 'a') as file:
        file.write(astr)


def sendOscMessage(prepend, tweet, delay=0):
    """
    Converts a tweet object into an OSC message

    /tweet, keyword, sent, retweet, location

    INPUTS
    -----------------

    OUTPUTS
    -----------------

    """
    time.sleep(delay)
    msg = osc_message_builder.OscMessageBuilder(prepend)

    # we only send the first index as this function will only be called on tweets which
    # matched only a single term
    if len(tweet.terms) > 1:
        terms = ''
        for term in tweet.terms:
            terms += term + " "
    else:
        terms = tweet.terms[0]

    msg.add_arg(terms, 's')
    msg.add_arg(tweet.sentiment, 'f')
    msg.add_arg(tweet.retweeted, "i")
    msg.add_arg(tweet.distance, "f")
    tweet.strength = determineTweetStrength(tweet)
    msg.add_arg(tweet.strength, 'f')
    built_message = msg.build()
    for client in clients:
        client.send(built_message)


    stringrep = buildOscString(prepend, tweet)
    addStringToFile(stringrep)

    if args.verbose is True:
        print(stringrep.rstrip())


def determineTurbulence(mode=0):
    """
    This function calcuulates the turbulance rating for the system

    Currently the function will calculate this value as follows:


    mode 0 = ((# original tweets) * 2) / (total_number of tweets + 1) * (highest_sent - lowest_sent)
                all divided by 4 to normalize into a range of 0-1

    mode 1 =

    mode 2 =


    INPUTS
    ------
    NONE

    OUTPUTS
    ------
    turb    `float 0-1
            how "turbulant" the twitter searches have been
    """
    turb = 0.0
    if mode == 0:
        og_tweets = 0
        low_sent = 0.0
        high_sent = 0.0
        for tweet in rescent_tweets:
            if tweet.retweeted == 0:
                og_tweets += 1
            if tweet.sentiment > high_sent:
                high_sent = tweet.sentiment
            elif tweet.sentiment < low_sent:
                low_sent = tweet.sentiment

        # base is 2x the number of OG tweets / num rescent_tweets + 1
        base = (og_tweets  * 2) / ( len(rescent_tweets) + 1 )

        turb = base * (high_sent - low_sent)
        # normalize to a range betweet 0-1
        # 4 is the highest reasonible number to expect
        turb = min(turb, 4.0) * 0.25
    return turb


def sendTurbulenceOscMessage(turb, delay=0):
    """
    WARNING, currently this function is not called by any other code
    """
    time.sleep(delay)
    msg = osc_message_builder.OscMessageBuilder("/turbulence")
    msg.add_arg(turb, 'f')
    built_message = msg.build()
    for client in clients:
        client.send(built_message)
    msg_string = "/turbulence," + str(turb) + "\n"
    addStringToFile(msg_string)
    if args.verbose is True:
        print("sent /turbulence osc message: ", turb)


def sendSpeedOscMessage(speed, delay=0):
    """
    Sends out /speed OSC messages. These messages contain a single floating point
    number which corresponds to the the relative "speed" of matched tweets

    INPUTS
    ------
    speed       `float
                the relative amount of matching tweet activity

    OUTPUTS
    -------
    NONE
    """
    time.sleep(delay)
    msg = osc_message_builder.OscMessageBuilder("/speed")
    msg.add_arg(speed, 'f')
    built_message = msg.build()
    for client in clients:
        client.send(built_message)
    msg_string = "/speed," + str(speed) + "\n"
    addStringToFile(msg_string)
    if args.verbose is True:
        print("sent /speed osc message: ", speed)


def isMegaTweet(tweet):
    """
    Searches through the text contained in a tweet to see if a hashed term
    is present. If one is the function will return True, if there is not
    any hashtagged terms present, the function will return False.

    INPUTS
    ------
    tweet   `tweet object - the tweet to search for hash-tagged keyterms

    OUTPUTS
    -------
    Bool    True if hastagged term is present and False if no hashtagged keyterms are
            present
    """
    key_terms = ["#ifsowhat", "#porsche", "#calarts", "#Porsche", "#CalArts", "#Calarts"]
    if tweet.retweeted == 0:
        for term in key_terms:
            if term in tweet.text.lower():
                return True
    return False


def sendCorrectOscMessage(tweet):
    """
    This function looks into the information contained in a tweet object
    and determines what is the appropiate OSC message to send

    INPUTS
    ---------------
    tweet           tweet object

    OUTPUTS
    ---------------
    returns 0 is no OSC message is sent
    returns 1 if a simple OSC message is sent
    returns 2 if a MEGA osc message is sent
    """
    if args.dontblock is False:
        rate_limited = rateLimit(tweet)
    else:
        rate_limited = False

    if args.oldmegas is True and rate_limited is False:
        # if only a single term matched
        if len(tweet.terms) == 1:
            print("> > > basic match : ", str(tweet.strength)[:4], "\t", tweet.text[:100])
            rescent_tweets.append(tweet)
            sendSpeedOscMessage(determineTweetSpeed(rescent_tweets))
            sendTurbulenceOscMessage(determineTurbulence())
            sendOscMessage('/tweet', tweet)
        elif tweet.distance < 11.0 and tweet.distance > -1.0:
            print("&&&&& tweeted from ", tweet.distance, " miles away: ",
                    str(tweet.strength)[:4], "\t", tweet.text[:100])
            rescent_tweets.append(tweet)
            sendSpeedOscMessage(determineTweetSpeed(rescent_tweets))
            sendTurbulenceOscMessage(determineTurbulence())
            sendOscMessage('/megatweet', tweet)
        elif len(tweet.terms) > 1:
            print(">>>>> multi match ", tweet.terms, ", ", str(tweet.strength)[:4], "\t",
                    tweet.text[:100])
            rescent_tweets.append(tweet)
            sendSpeedOscMessage(determineTweetSpeed(rescent_tweets))
            sendTurbulenceOscMessage(determineTurbulence())
            sendOscMessage('/megatweet', tweet)
    elif rate_limited is False:
        # if the searched term is a hastag term send a Mega OSC message
        if isMegaTweet(tweet) is True:
            print("+++++ mega tweet     : ", str(tweet.strength)[:4],"\t",
                    tweet.text[:100])
            rescent_tweets.append(tweet)
            sendSpeedOscMessage(determineTweetSpeed(rescent_tweets))
            sendTurbulenceOscMessage(determineTurbulence())
            sendOscMessage('/megatweet', tweet)
            if args.multitweet > 1:
                t = Thread(target=sendOscMessage, args=('/megatweet', tweet, 5))
                t.start()
                if args.multitweet > 2:
                    t = Thread(target=sendOscMessage, args=('/megatweet', tweet, 10))
                    t.start()

        elif len(tweet.terms) > 0:
            print("> > > matching tweet : ", str(tweet.strength)[:4],"\t", tweet.text[:100])
            rescent_tweets.append(tweet)
            sendSpeedOscMessage(determineTweetSpeed(rescent_tweets))
            sendTurbulenceOscMessage(determineTurbulence())
            sendOscMessage('/tweet', tweet)
            if args.multitweet > 1:
                t = Thread(target=sendOscMessage, args=('/tweet', tweet, 5))
                t.start()
                if args.multitweet > 2:
                    t = Thread(target=sendOscMessage, args=('/tweet', tweet, 10))
                    t.start()

    # to keep track that this tweet was added to the system
    if args.verbose:
        print("- - - - - - - - - - - ")
    used_tweets.append(tweet)


def determineTweetSpeed(tweets):
    """
    Calculates the relative speed of incomming tweets in terms of
    tweets per minute

    INPUTS
    ------
    tweets              `list of tweet objects

    OUTPUTS
    -------
    t_per_minute        `float
                        The number of tweets which send out OSC messages recognized
                        by the system per minute
    """
    num = 0
    total = 0
    for tweet in tweets:
        if tweet.last_time is not None:
            num += 1
            total += tweet.last_time
    if num > 0:
        time_between = total/num
        t_per_minute = 60/time_between
        return t_per_minute
    else:
        return 1.0


def rateLimit(tweet):
    """
    return false if tweet should not be tweeted and true if it should
    """
    times_matched = 1
    last_tweet = 600
    if tweet.retweeted > 0:
        # check all of the tweets we used to send OSC messages in
        # the last ten minutes
        for t in rescent_tweets:
            # if there are previous retweets of the same original tweet increment
            # times_matched by one
            if str(t.text).lower() in str(tweet.text).lower():
                times_matched += 1
                # set last_tweet to the number of seconds ago in which the last retweet
                # was retweeted
                if tweet.time - t.time < last_tweet:
                    last_tweet = tweet.time - t.time

    # the min number of seconds that has to have passed before this function will let
    # another tweet through
    time_limit = times_matched * times_matched * 5
    if last_tweet < time_limit and args.dontblock is False:
        print("xxxxx RETWEET BLOCKED DUE TO HYPER ACTIVITY - ", last_tweet," - ", time_limit,
                " - ", tweet.text[:100])
        return True

    return False


def parseCommandLineArgs():
    """
    This utility function sets up an argument parser for command line arguments to the
    script. It returns a dict containing key value pairs consisting of the command line
    arguments passed into the program.
    """
    parser = argparse.ArgumentParser(description="""This program searches for
            tweets containing select keywords and then sends OSC messages
            containing information about the location of the tweet, the
            matching keyword and other chunks of information.

            Example uses:

            python3 streaming-listener.py
            python3 streaming-listener.py -ip 167.56.345 -port 5000 -v
            python3 streaming-listener.py -terms love hate happy -vv
            """)

    parser.add_argument("-ip", default="127.0.0.1",
        help="The ip of the OSC server. Default is 127.0.0.1. Please note that the\
        program will always send the OSC messags to 127.0.0.1 in addition to the \
        adress entered in with this flag")
    parser.add_argument("-port", type=int, default=7000,
        help="The listening port of the OSC server. Default is 7000.")
    parser.add_argument("-terms",
            default=['porsche', 'ifsowhat', 'calarts'],
            nargs="+",
            help="The keyterms to search for. Default is: porsche ifsowhat calarts")
    parser.add_argument("-sterms", default=['cayman'], nargs="+", help="\
            Secondary terms which will increase a tweets strength, but will\
            not be searched for directly. Default is: cayman")
    parser.add_argument("-r", "--removeretweets", action="store_true",
        help="The program will only return original tweets, all retweets will be discarded\
              .")
    parser.add_argument("-m", "--multitweet", type=int, default=1,
        help="Multiple OSC messages per tweet. If 3 is passed in along with this flag the\
              program will output 3 OSC messages (5 second apart).")
    parser.add_argument("-termlimit", type=int, default=20000,
        help="The program will rate limit each term to a number of\
        messages for every 10 minutes. This is equal to the value passed in with this\
        flag. Default is 2000")
    parser.add_argument("-o", "--oldmegas", action="store_true",
        help="Will use the old logic for megatweets (location, and multiple terms \
              returned) instead of the new logic (looking for #'s)")
    parser.add_argument("-v", "--verbose", action="store_true",
        help="Prints extra information including the text of tweets "+
              " which are triggering the OSC messages.")
    parser.add_argument("-d", "--dontblock", action="store_true",
        help="Prevents the program from blocking tweets due to hyperactivity.")
    return parser.parse_args()


def determineValidTerms():
    """
    """
    blocked_terms = [0 for t in args.terms]
    for tweet in rescent_tweets:
        for i, term in enumerate(args.terms):
            if term in tweet.terms:
                blocked_terms[i] += 1

    terms = []
    for i, term_num in enumerate(blocked_terms):
        if term_num > args.termlimit:
            terms.append(args.terms[i])
            print("term is overloaded : ", terms[-1])

    valid_terms = []
    for term in args.terms:
        if term not in terms:
            valid_terms.append(term)

    return valid_terms


class StreamListener(tweepy.StreamListener):
    """
    TODO
    """

    def on_status(self, tweet):
        tweet.retweeted = tweetUtils.isRetweeted(tweet)
        if args.removeretweets is True and tweet.retweeted == 1:
            if args.verbose is True:
                print("xxxxx rejecting retweet : ", tweet.text[:100])
            return 0
        tweet.distance = tweetUtils.findTweetLocation(tweet, pofa_loc)
        tweet.og_text = tweet.text # backup the tweets original text for the "strength"
        # function
        tweet.text = sentiment.filterTweetText(tweet.text)
        tweet.sentiment = getTweetSentiment(tweet)
        terms = determineValidTerms()
        tweet.terms = tweetUtils.findTweetTerms(tweet, terms)
        tweet.strength = determineTweetStrength(tweet)
        tweet.time = time.time()
        if len(rescent_tweets) > 0:
            tweet.last_time = tweet.time - rescent_tweets[-1].time
        else:
            tweet.last_time = None

        if len(tweet.terms) > 0:
            sendCorrectOscMessage(tweet)
        elif args.verbose is True:
            print("xxxxx rejecting tweet for not containing allowed keyterms: ",
                    tweet.text[:100])
            print("-----------------------------------")

    def on_error(self, status_code):
        print(status_code)
        if status_code == 420:
                return False

    def onValueChange(par, val, prev):

        global newTweets

        if par.name == 'Enabled' and val == 'off':
            disconnect()
            return
        elif par.name == 'Enabled' and val == 'on':
            connect()
            return

        if len(newTweets) > 0:
            print(newTweets)
            for tweet in newTweets:
                for keyword in wordlist:
                    if keyword in tweet:
                        print("!!! "+keyword + " !!!"+ op('wordList').cell(keyword,'strength'))
                        op('porscheTrigger').par.triggerpulse.pulse()
            newTweets = []

    def disconnect():
        stream.disconnect()
        print("disconnected...")
        return

    def onPulse(par):
        if par.name == 'Refresh' and op('twitterCtrls').par.Enabled == True:
                disconnect()
                connect()
                return
        return

    def createStream():
        """TODO"""
        # moved the keys and secrets to a different file for security (should be .gitignored in near future)
        auth = tweepy.OAuthHandler(TWITTER_APP_KEY, TWITTER_APP_SECRET)
        auth.set_access_token(TWITTER_KEY, TWITTER_SECRET)
        api = tweepy.API(auth)
        stream_listener = StreamListener()
        stream = tweepy.Stream(auth=api.auth, listener=stream_listener)
        return stream


if __name__ == "__main__":
    # Coordinates of the Palace of Fine Arts in SF W/N
    pofa_loc = [-122.4492, 37.8030]

    # the tweets we have already sent OSC messages for
    rescent_tweets = []
    used_tweets = []
    new_tweets = []

    stream = StreamListener.createStream()

    # parse the command line arguments
    args = parseCommandLineArgs()

    #
    blocked_terms = []

    # create a client which can send OSC messages
    # the ip and port are set with command line arguments
    clients  = []
    clients.append(udp_client.UDPClient(args.ip, args.port, True))
    if args.ip != "127.0.0.1":
        clients.append(udp_client.UDPClient("127.0.0.1", args.port, True))
        print(clients)


    print("| | | | | | | | | | | | | | | | | | | | | | | | | | ")
    print("starting script using the following arguments:")
    print(args)
    print("| | | | | | | | | | | | | | | | | | | | | | | | | | ")

    now = time.localtime(time.time())
    file_name = path.join("logs", str(now.tm_year) + "-" + str(now.tm_mon) + \
            "-" + str(now.tm_mday) + "_" + str(now.tm_hour) + \
            "-" + str(now.tm_min) + ".txt")
    # create the log file
    with open(file_name,  'w+') as f:
       f.write(args.__str__() + "\n")
       print("created log file : ", file_name)

    stream.filter(track=args.terms, async=True)
    while True:
        used_tweets = adjustStoredTweetList(used_tweets)
        rescent_tweets = tweetUtils.removeOldTweets(rescent_tweets)

