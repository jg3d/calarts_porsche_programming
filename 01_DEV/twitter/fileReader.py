import argparse
import time
from pythonosc import osc_message_builder
from pythonosc import udp_client


def addStringToFile(astr):
    """
    TODO
    """
    with open(file_name, 'a') as file:
        file.write(astr)


def sendOscMessage(prepend, tweet):
    """
    Converts a tweet object into an OSC message

    /tweet, keyword, sent, retweet, location

    INPUTS
    -----------------

    OUTPUTS
    -----------------

    """
    msg = osc_message_builder.OscMessageBuilder(prepend)

    # we only send the first index as this function will only be called on tweets which
    # matched only a single term
    print(tweet)
    msg.add_arg(tweet[2], 's')
    msg.add_arg(float(tweet[3]), 'f')
    msg.add_arg(int(tweet[4]), "i")
    msg.add_arg(float(tweet[5]), "f")
    msg.add_arg(float(tweet[6]), 'f')

    built_message = msg.build()
    client.send(built_message)

    if args.verbose is True:
        print("sent", prepend, tweet[2], tweet[3],\
                tweet[4], tweet[5], float(tweet[6]))


def sendTurbulenceOscMessage(turb):
    """
    WARNING, currently this function is not called by any other code
    """
    msg = osc_message_builder.OscMessageBuilder("/turbulence")
    msg.add_arg(turb, 'f')
    built_message = msg.build()
    client.send(built_message)
    if args.veryverbose is True:
        print("sent /turbulence osc message: ", turb)


def sendSpeedOscMessage(speed):
    """
    Sends out /speed OSC messages. These messages contain a single floating point
    number which corresponds to the the relative "speed" of matched tweets

    INPUTS
    ------
    speed       `float
                the relative amount of matching tweet activity

    OUTPUTS
    -------
    NONE
    """
    msg = osc_message_builder.OscMessageBuilder("/speed")
    msg.add_arg(speed, 'f')
    built_message = msg.build()
    client.send(built_message)
    if args.veryverbose is True:
        print("sent /speed osc message: ", speed)


def parseCommandLineArgs():
    """
    This utility function sets up an argument parser for command line arguments to the
    script. It returns a dict containing key value pairs consisting of the command line
    arguments passed into the program.
    """
    parser = argparse.ArgumentParser(description="""
            This script is for reading back .txt files created by the
            twitterListener.py script. It will read back the .txt files which act as
            recordings of which tweet happened at which time. To run the script you have
            to pass a filename in. A example call would be: python3 file-reader.py
            --filename 2018-4-9_17-6.txt""")

    parser.add_argument("-f", "--filename", help="The file name that the program will\
            read. IMPORTANT: this argument has to be passed into the script or else\
            nothing will happen. (see above)")
    parser.add_argument("-ip", default="127.0.0.1",
        help="The ip of the OSC server. Default is 127.0.0.1")
    parser.add_argument("-port", type=int, default=7000,
        help="The listening port of the OSC server. Default is 7000.")
    parser.add_argument("-v", "--verbose", action="store_true",
        help="Prints extra information including the text of tweets "+
              " which are triggering the OSC messages.")
    parser.add_argument("-vv", "--veryverbose", action="store_true",
        help="Prints the most information, everything that verbose prints plus more")
    return parser.parse_args()


if __name__ == "__main__":

    # Coordinates of the Palace of Fine Arts in SF W/N
    pofa_loc = [-122.4492, 37.8030]

    # the tweets we have already sent OSC messages for
    rescent_tweets = []
    used_tweets = []
    new_tweets = []

    # parse the command line arguments
    args = parseCommandLineArgs()

    # the name of the file is first argument
    print("starting script using the following arguments:")
    print(args)

    # create a client which can send OSC messages
    # the ip and port are set with command line arguments
    client = udp_client.UDPClient(args.ip, args.port, True)

    if args.veryverbose is True:
        args.verbose = True

    if args.filename is None:
        print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||")
        print("ERROR - No file name given. ")
        print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||")
        print("Please run the script using the -f" +\
                " flag followed by the path to a valid log .txt file.")
        print("e.g. : python3 fileReader.py -f logs/2018-4-20-3-40.txt")
        print("for more info run this script with the -h (help) flag")
        print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||")
    else:
        with open(args.filename, 'r') as file:
            tweet_data = file.readlines()
            file_params = tweet_data[0]
            print("file params : ", file_params)
            tweet_data.remove(file_params)
            for tweet in tweet_data:
                print("tweet : ", tweet)
                comps = tweet.split(",")
                if comps[1] == '/tweet' or comps[1] == '/megatweet':
                    if comps[0] != 'None':
                        timel = float(comps[0])
                        sendOscMessage(comps[1], comps)
                        time.sleep(timel)
                elif comps[0] == "/turbulence":
                    sendTurbulenceOscMessage(float(comps[1]))
                elif comps[0] == "/speed":
                    sendSpeedOscMessage(float(comps[1]))
