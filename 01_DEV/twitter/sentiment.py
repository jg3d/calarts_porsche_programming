from textblob import TextBlob


def tweetSentiment(tweet, verbose=False):
    """
    This function analyzes a tweet which it takes in as an input as either a
    tweet object or a string. It breaks down the tweet into its sentances and then analyzes the
    sentiment of each sentance on a sliding scale from -1 to 1.

    Currently the function then blindly averages the sentiments of each sentance
    and returns a float which corresponds to how "positive" or "negative" the tweet is.

    INPUTS
    ------------------
    tweet       str, textblob.blob.TextBlob, or tweet object
                The tweet which we are to analyze the sentient of

    verbose     boolean
                Should the funciton print out a bunch of infomation about
                what it is up to?

    RETURNS
    ------------------
    avg_sent    float
                What is the sentient of all included sentances averaged together?
                a negative number equates to a negative sentiment while a positive number
                equates to positive sentiment.

    TODO: add logic which ensures that the sentiment analysis is being performed in regard
    to our key_terms
    """
    text = translateTweets(tweet).text
    text = filterTweetText(text)
    blob = TextBlob(text)
    # lblob = TextBlob(lemmatizeTweet(text))
    # print(blob.sentences[0].polarity, "/",lblob.sentences[0].polarity)

    # this just allows for debug printing
    if verbose is True:
        print(blob.tags)
        print("------------------------------")
        print(blob.noun_phrases)
        print("------------------------------")
        print(len(blob.sentences), blob.sentences)
        print("------------------------------")
        for sent in blob.sentences:
            print(sent.sentiment.polarity)
        print("------------------------------")

    # we need to make sure that we double check the subject of each sentance

    # we also need to figure out how to deal with multiple sentences
    # right now the function is niavely adding them together
    avg_sent = 0
    for sent in blob.sentences:
        avg_sent += sent.sentiment.polarity
    if len(blob.sentences) > 0:
        avg_sent /= len(blob.sentences)
    else:
        avg_sent = 0

    if verbose is True:
        print("Returned sent : ", avg_sent)
        print("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||")

    return avg_sent


def translateTweets(tweets, verbose=False):
    """
    Takes in a list of tweets and then translates the ones which are not english into english
    Note that this function can now take in either a single tweet object or a list of
    tweet objects

    INPUTS
    --------------
    tweet       str, textblob.blob.TextBlob, or tweet object
                The tweet which we are to analyze the sentient of

    verbose     boolean
                Should the funciton print out a bunch of infomation about
                what it is up to?

    RETURNS
    -------------
    tweets      tweet obects
                The list of tweets passed into the function with translation applied
    """
    if type(tweets) is list:
        for tweet in tweets:
            if tweet.lang != 'en':
                blob = TextBlob(tweet.text)
                try:
                    tweet.text = blob.translate(to='en')
                    tweet.lang = 'en'
                    if verbose == True:
                        print(tweet.text)
                except Exception as NotTranslated:
                    if verbose == True:
                        print("Could not translate! ", tweet.text)
    else:
        if tweets.lang != 'en':
            blob = TextBlob(tweets.text)
            try:
                tweets.text = blob.translate(to='en')
                tweets.lang = 'en'
                if verbose == True:
                    print(tweets.text)
            except Exception as NotTranslated:
                if verbose == True:
                    print("Could not translate! ", tweets.text)

    return tweets


def lemmatizeTweet(tweet):
    normalized_tweet = ""
    text = TextBlob(tweet.lower())
    for word in text.words:
        normalized_tweet += word.lemmatize() + " "
    return normalized_tweet


def removeUnwantedWords(text):
    """
    Also removes links
    """
    new = ""
    for word in text.split(" "):
        word = word.replace('\n', ' ').strip('\n')
        if "\n" in word:
            print("newline found! : ", word)
        if not word.startswith("@") and not word.startswith("http"):
            new = new + word.rstrip() + " "
    return new


def filterTweetText(text):
    """
    This function is intended to perform NL processing that
    is specific to tweet texts.

    This includes removing RT from the start of tweets and the removal of '@'
    """
    new = text
    if new.startswith("RT "):
        new = text[3:]
    new = removeUnwantedWords(new)
    return new


