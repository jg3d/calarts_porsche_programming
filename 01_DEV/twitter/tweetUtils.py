from geopy import distance as gdist
import time


def findTweetIndexById(_id, tweets):
    """
    searches through a list of tweets for a tweet with an
    id attribute which matches _id

    INPUTS
    ------------
    _id         tweet id
    tweets      list of tweet objects

    OUTPUTS
    ------------
    i           tweet id
                the index number of the tweet which matches
                _id. will return None if no tweet matches
    """
    for i, tweet in enumerate(tweets):
        if tweet.id == _id:
            return i
    return None


def findTweetTerms(tweet, terms):
    """
    searches through the text in a tweet for keywords, returns
    a list of all keywords found in the tweet

    INPUTS
    ------------
    tweet           tweet object
    terms           list of strings corresponding to words or phraises the
                        script is searching for

    OUTPUTS
    ------------
    tweet_terms     list of tweet terms which we found in the text of tweet
    """
    tweet_terms = []
    for term in terms:
        if term.lower() in tweet.text.lower():
            tweet_terms.append(term)
    return tweet_terms


def removeUsedTweets(tweets):
    """
    removes any tweets from a list of tweets which have been used in
    rescently to send OSC messages

    INPUTS
    ------------
    tweets          list of tweet objects

    OUTPUTS
    ------------
    good_tweets     list of tweet objects
    """
    good_tweets = []
    used_tweet_ids = [t.id for t in used_tweets]
    for tweet in tweets:
        if tweet.id not in used_tweet_ids:
            good_tweets.append(tweet)
    return good_tweets


def removeDuplicateTweets(tweets):
    """
    removes any tweets from a list of tweets which are the same
    as another tweet in the list (keeps one single copy). returns
    a version of the list without duplicates.

    INPUTS
    ------------
    tweets          list of tweet objects

    OUTPUTS
    ------------
    good_tweets     list of tweet objects
    """
    good_tweets = []
    for tweet in tweets:
        found = 0
        for t in tweets:
            if t.id == tweet.id:
                found = found + 1
        if found == 1:
            good_tweets.append(tweet)
    return good_tweets


def removeOldTweets(tweets, max_time=600):
    """
    TODO
    """
    now = time.time()
    for tweet in tweets:
        # if the tweet occured a number of seconds in the past greater than max_time
        if now - tweet.time > max_time:
            tweets.remove(tweet)
    return tweets


def findTweetLocation(tweet, target):
    """
    """
    coords = None
    distance = -1.0
    if tweet.coordinates is not None:
        coords = tweet.coordinates['coordinates']
        # print("coords : ", coords)
    elif tweet.geo is not None:
        coords = tweet.geo['coordinates']
        # print("geo coords : ", coords)
    elif tweet.place is not None:
        # a place returns a bounding box,
        # which contains coordinates for the four corners"
        coords = getCenterOfPlace(tweet.place.bounding_box.coordinates)
        # print("center of place coords : ", coords)

    # if we are unable to find location information set distance to -1.0
    if coords is not None:
        distance = gdist.distance(coords, target).miles

    return distance


def determineTweetDirection():
    """
    TODO

    for tweets which have a lat and long this function determines which
    direction the tweeter tweeted the tweet from.
    """


def getCenterOfPlace(boundingBox):
    """
    This utility function will figure out the center point of a bounding box
    it is intended to be used to figure out a single location that can be used for twitter
    places

    INPUTS
    ----------------
    boundingBox         list of lists containing two int elements which correspond with a
                        lat and long

    OUTPUTS
    ----------------
    the_loc             list containing tw ints which correspond with a lat and long of
                        the center of the bounding box

    """
    the_loc = [None, None]
    if type(boundingBox) is list and type(boundingBox[0]) is list and len(boundingBox[0]) == 4:
        x = 0
        y = 0
        for loc in boundingBox[0]:
            x += loc[0]
            y += loc[1]
        the_loc[0] = x * 0.25
        the_loc[1] = y * 0.25
    return the_loc


def isRetweeted(tweet):
    if tweet.text.startswith("RT"):
        return 1
    return 0
